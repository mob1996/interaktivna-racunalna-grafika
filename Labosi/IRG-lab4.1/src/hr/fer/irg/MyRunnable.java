package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import static java.lang.Math.sqrt;
import static java.lang.Math.pow;

/**
 * Created by Luka on 4.5.2017..
 */
public class MyRunnable implements Runnable{


    private static ArrayList<Vertex3Df> vertices = new ArrayList<>();
    public static ArrayList<TrianglePolygon> polygons = new ArrayList<>();
    public static ArrayList<Vertex3Df> bezierVertices = new ArrayList<>();
    private double xmin = Double.MAX_VALUE;
    private double xmax = Double.MIN_VALUE;
    private double ymin = Double.MAX_VALUE;
    private double ymax = Double.MIN_VALUE;
    private double zmin = Double.MAX_VALUE;
    private double zmax = Double.MIN_VALUE;
    private boolean fitScreen=false;

    public static Vertex3Df O = new Vertex3Df(3,3,3);
    public static Vertex3Df G = new Vertex3Df(0,0,0);
    public static Vertex3Df L = new Vertex3Df(1,4,8);

    public static boolean shading = false;
    public static boolean constantShading = true;
    public static boolean gouraudShading = false;

    @Override
    public void run() {
        readObjectFile();
        calculatePolyKoefs();
        checkIfHidden();
        calculateAverageNormals();
        vertices = new ArrayList<>();
        initGUI();
    }

    private void initGUI(){
        JFrame frame = new JFrame("Exercise 4.1.");
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLCanvas glCanvas =  new GLCanvas(glCapabilities);

        glCanvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if(e.getKeyChar()=='o'){
                    shading=!shading;
                } if(e.getKeyChar()=='p'){
                    constantShading=!constantShading;
                    gouraudShading=!gouraudShading;
                }
                glCanvas.display();
            }
        });

        glCanvas.addGLEventListener(new MyGLEventListener());
        frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
        frame.setSize(800,800);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });
    }


    public void readObjectFile(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("F:\\Faks\\Interaktivna računalna grafika\\Labosi\\IRG-lab4.1\\kocka.obj"));
            String line;
            while((line=reader.readLine())!=null){
                if(!(line.startsWith("v") || line.startsWith("f"))){
                    continue;
                }
                String[] tokens = line.split("\\s+");
                if(tokens[0].equals("v")){
                    processVertex(tokens);
                } else if(tokens[0].equals("f")){
                    if(fitScreen==false){
                        pointsFitScreen();
                        fitScreen=true;
                    }
                    processPolygon(tokens);
                }

            }
            reader.close();
        } catch (Exception e){
            System.out.println("Greska pri ocitavanju objekta");
            e.printStackTrace();
        }

    }

    private void processVertex(String[] tokens){
        float x = Float.valueOf(tokens[1]);
        float y = Float.valueOf(tokens[2]);
        float z = Float.valueOf(tokens[3]);
        if(x<xmin) xmin=x;
        if(x>xmax) xmax=x;
        if(y<ymin) ymin=y;
        if(y>ymax) ymax=y;
        if(z<zmin) zmin=z;
        if(z>zmax) zmax=z;

        Vertex3Df vertex = new Vertex3Df(x,y,z);
        vertices.add(vertex);
    }

    private void processPolygon(String[] tokens){
        int indexOfFirst = Integer.valueOf(tokens[1].split("\\.")[0])-1;
        int indexOfSecond = Integer.valueOf(tokens[2].split("\\.")[0])-1;
        int indexOfThird =  Integer.valueOf(tokens[3].split("\\.")[0])-1;
        TrianglePolygon trianglePolygon = new TrianglePolygon(vertices.get(indexOfFirst), vertices.get(indexOfSecond), vertices.get(indexOfThird));
        vertices.get(indexOfFirst).addPolygon(trianglePolygon);
        vertices.get(indexOfSecond).addPolygon(trianglePolygon);
        vertices.get(indexOfThird).addPolygon(trianglePolygon);
        polygons.add(trianglePolygon);
    }

    private void pointsFitScreen(){
        double velicina_x = xmax-xmin;
        double velicina_y = ymax-ymin;
        double velicina_z = ymax-ymin;

        double maxVelicina = velicina_x;
        if(velicina_y>maxVelicina) maxVelicina=velicina_y;
        if(velicina_z>maxVelicina) maxVelicina=velicina_z;

        double scaleValue = 2/maxVelicina;

        double srediste_x = (xmax+xmin)/2;
        double srediste_y = (ymax+ymin)/2;
        double srediste_z = (zmax+zmin)/2;

        double[][] translate = {{1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {-srediste_x,-srediste_y,-srediste_z, 1}};
        double[][] scale = {{scaleValue, 0, 0, 0},
                {0, scaleValue, 0, 0},
                {0, 0, scaleValue, 0},
                {0, 0, 0, 1}};
        RealMatrix transformPoints = new Array2DRowRealMatrix(translate);
        transformPoints=transformPoints.multiply(new Array2DRowRealMatrix(scale));
        for(Vertex3Df v:vertices){
            double x = v.getX();
            double y = v.getY();
            double z = v.getZ();
            double[][] point = {{x,y,z,1}};
            RealMatrix pointMatrix = new Array2DRowRealMatrix(point);
            pointMatrix = pointMatrix.multiply(transformPoints);
            point = pointMatrix.getData();
            float h = (float)point[0][3];
            v.setX((float)point[0][0]/h);
            v.setY((float)point[0][1]/h);
            v.setZ((float)point[0][2]/h);

        }

    }

    private void calculatePolyKoefs(){
        for(TrianglePolygon tp:polygons){
            tp.calculateKoefs();
            //System.out.println(tp.getKoefA()+"   "+tp.getKoefB()+"   "+tp.getKoefC()+"   "+tp.getKoefD());
        }
    }

    private void checkIfHidden(){
        for(TrianglePolygon tp:polygons){
            double[] normVector = {tp.getKoefA(), tp.getKoefB(), tp.getKoefC()};
            double[] cToO = {O.getX()-tp.getCenterX(), O.getY()-tp.getCenterY(), O.getZ()-tp.getCenterZ()};
            double norm1=0;
            double norm2=0;
            for(int i=0; i<3;i++){
                norm1+=pow(normVector[i],2);
                norm2+=pow(cToO[i],2);
            }
            norm1=sqrt(norm1);
            norm2=sqrt(norm2);
            for(int i=0; i<3;i++){
                normVector[i]=normVector[i]/norm1;
                cToO[i] =  cToO[i]/norm2;
            }
            double product=0;
            for(int i=0; i<3;i++){
                product+=(normVector[i]*cToO[i]);
            }
            if(product>0){
                tp.setHidden(false);
            } else {
                tp.setHidden(true);
            }
        }
    }

    private void calculateAverageNormals(){
        for(Vertex3Df v:vertices){
            v.calculateAverageNormal();
        }
    }

}


