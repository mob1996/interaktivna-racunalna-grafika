package hr.fer.irg;

import java.util.ArrayList;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class Vertex3Df {
    private float x;
    private float y;
    private float z;
    private double[] avgNormal = {0,0,0};

    private ArrayList<TrianglePolygon> polygons = new ArrayList<>();

    public Vertex3Df(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public double[] getAvgNormal(){
        return avgNormal;
    }

    public void addPolygon(TrianglePolygon tp){
        polygons.add(tp);
    }

    public void calculateAverageNormal(){
        for(TrianglePolygon tp:polygons){
            avgNormal[0] += tp.getKoefA();
            avgNormal[1] += tp.getKoefB();
            avgNormal[2] += tp.getKoefC();
        }
        for(int i=0;i<avgNormal.length;i++){
            avgNormal[i] = avgNormal[i]/polygons.size();
        }
    }

    @Override
    public String toString(){
        return "v " + x + " " + y + " " + z;
    }
}
