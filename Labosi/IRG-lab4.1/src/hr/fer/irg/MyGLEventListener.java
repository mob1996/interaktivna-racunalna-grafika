package hr.fer.irg;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;

import static java.lang.Math.sqrt;
import static java.lang.Math.pow;

/**
 * Created by Luka Mijić on 14.5.2017..
 */
public class MyGLEventListener implements GLEventListener {
    private GLU glu = new GLU();

    private double Ia = 100;
    private double ka = 0.5;

    private double Ii = 200;
    private double kd = 0.8;


    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClearColor(1,1,1,0);
        gl2.glClearDepth(1.0f);
        gl2.glEnable(GL.GL_DEPTH_TEST);
        gl2.glDepthFunc(GL.GL_LEQUAL);
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClear (GL2.GL_COLOR_BUFFER_BIT |  GL2.GL_DEPTH_BUFFER_BIT );
        gl2.glLoadIdentity();
        glu.gluLookAt(MyRunnable.O.getX(), MyRunnable.O.getY(), MyRunnable.O.getZ(),0,0,0,0,1,0);
        if(!MyRunnable.shading){
            renderLinesObj(gl2);
        } else {
            if(MyRunnable.constantShading){
                renderConstantShadingObj(gl2);
            } else {
                renderGouraudShadingObj(gl2);
            }
        }

    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glViewport(0,0,width,height);
        gl2.glEnable(GL2.GL_DEPTH_TEST);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        gl2.glFrustum(-1.5, 1.5, -1.5, 1.5, 1.0, 100.0);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);

    }




    private void renderLinesObj(GL2 gl2){
        gl2.glColor3f(1,0,0);
        gl2.glPointSize(1.0f);
        for(TrianglePolygon tp:MyRunnable.polygons){
            if(tp.isHidden()){
                continue;
            }
            gl2.glBegin(GL2.GL_LINE_LOOP);
            gl2.glVertex3f(tp.getFirst().getX(),
                    tp.getFirst().getY(),
                    tp.getFirst().getZ());
            gl2.glVertex3f(tp.getSecond().getX(),
                    tp.getSecond().getY(),
                    tp.getSecond().getZ());
            gl2.glVertex3f(tp.getThird().getX(),
                    tp.getThird().getY(),
                    tp.getThird().getZ());
            gl2.glEnd();
        }
    }

    private void renderConstantShadingObj(GL2 gl2){
        for(TrianglePolygon tp:MyRunnable.polygons){
            if(tp.isHidden()){
                continue;
            }
            double[] normal = {tp.getKoefA(), tp.getKoefB(), tp.getKoefC()};
            normal = normalize(normal);
            double[] cToL = {MyRunnable.L.getX()-tp.getCenterX(), MyRunnable.L.getY()-tp.getCenterY(), MyRunnable.L.getZ()-tp.getCenterZ()};
            cToL = normalize(cToL);

            double product = vectorProduct(normal,cToL);

            double Id = Ii*kd*product;
            Id = max(Id, 0);

            double I = Ia*ka+Id;

            gl2.glColor3f((float)(I/256.0),0,0);

            gl2.glBegin(GL2.GL_TRIANGLES);
            gl2.glVertex3f(tp.getFirst().getX(),
                    tp.getFirst().getY(),
                    tp.getFirst().getZ());
            gl2.glVertex3f(tp.getSecond().getX(),
                    tp.getSecond().getY(),
                    tp.getSecond().getZ());
            gl2.glVertex3f(tp.getThird().getX(),
                    tp.getThird().getY(),
                    tp.getThird().getZ());
            gl2.glEnd();

        }
    }

    private void renderGouraudShadingObj(GL2 gl2){
        for(TrianglePolygon tp:MyRunnable.polygons){
            if(tp.isHidden()){
                continue;
            }
            gl2.glBegin(GL2.GL_TRIANGLES);
            gl2.glColor3f((float)(calculateI(tp.getFirst())/256.0),0,0);
            gl2.glVertex3f(tp.getFirst().getX(),
                    tp.getFirst().getY(),
                    tp.getFirst().getZ());
            gl2.glColor3f((float)(calculateI(tp.getSecond())/256.0),0,0);
            gl2.glVertex3f(tp.getSecond().getX(),
                    tp.getSecond().getY(),
                    tp.getSecond().getZ());
            gl2.glColor3f((float)(calculateI(tp.getThird())/256.0),0,0);
            gl2.glVertex3f(tp.getThird().getX(),
                    tp.getThird().getY(),
                    tp.getThird().getZ());
            gl2.glEnd();
        }
    }

    private double[] normalize(double[] vector){
        double norm=0;
        for(int i=0;i<vector.length;i++){
            norm+=pow(vector[i],2);
        }
        norm = sqrt(norm);
        for(int i=0;i<vector.length;i++){
            vector[i] = vector[i]/norm;
        }
        return vector;
    }

    private double vectorProduct(double[] v1, double[] v2){
        double product=0;
        for(int i=0;i<v1.length;i++){
            product+=(v1[i]*v2[i]);
        }
        return product;
    }

    private double max(double a, double b){
        if(a>=b){
            return a;
        } else {
            return b;
        }
    }

    private double[] vertexToL(Vertex3Df v){
        Vertex3Df L = MyRunnable.L;
        double[] vToL ={L.getX()-v.getX(), L.getY()-v.getY(), L.getZ()-v.getZ()};
        return vToL;
    }

    private double calculateI(Vertex3Df v){
        double I=0;
        double[] normalV = v.getAvgNormal();
        normalV = normalize(normalV);
        double[] vToL = vertexToL(v);
        vToL = normalize(vToL);

        double product  = vectorProduct(normalV,vToL);

        I= Ia*ka + max(Ii*kd*product,0);

        return I;
    }
}
