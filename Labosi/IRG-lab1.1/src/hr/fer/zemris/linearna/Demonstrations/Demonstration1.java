package hr.fer.zemris.linearna.Demonstrations;

import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;
import hr.fer.zemris.linearna.vectors.IVector;
import hr.fer.zemris.linearna.vectors.Vector;

/**
 * Created by Luka Mijić on 11.3.2017..
 */
public class Demonstration1 {

    public static void main(String[] args){
        try{
            IVector v1 = Vector.parseSimple("2 3 -4").add(Vector.parseSimple("-1 4 -3"));
            double s =  v1.scalarProduct(Vector.parseSimple("-1 4 -3"));
            IVector v2 = v1.nVectorProduct(Vector.parseSimple("2 2 4"));
            IVector v3 = v2.nNormalize();
            IVector v4 = v2.nScalarMultiply(-1);

            IMatrix m1 = Matrix.parseSimple("1 2 3 | 2 1 3 | 4 5 1").
                    add(Matrix.parseSimple("-1 2 -3 | 5 -2 7 | -4 -1 3"));
            IMatrix m2 = Matrix.parseSimple("1 2 3 | 2 1 3 | 4 5 1").
                    nMultiply(Matrix.parseSimple("-1 2 -3 | 5 -2 7 | -4 -1 3").nTransponse(false));
            IMatrix m3 = Matrix.parseSimple("-24 18 5 | 20 -15 -4 | -5 4 1").nInvert().
                    nMultiply(Matrix.parseSimple("1 2 3 | 0 1 4 | 5 6 0").nInvert());

            System.out.println(v1);
            System.out.println(s);
            System.out.println(v2);
            System.out.println(v3);
            System.out.println(v4);
            System.out.println(m1);
            System.out.println(m2);
            System.out.println(m3);
        } catch (Exception e){
            e.printStackTrace();
        }


    }
}
