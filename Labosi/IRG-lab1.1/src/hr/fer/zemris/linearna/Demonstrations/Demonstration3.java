package hr.fer.zemris.linearna.Demonstrations;

import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;
import hr.fer.zemris.linearna.matrix.MatrixVectorView;
import hr.fer.zemris.linearna.vectors.IVector;
import hr.fer.zemris.linearna.vectors.Vector;
import hr.fer.zemris.linearna.vectors.VectorMatrixView;

import java.util.Scanner;

/**
 * Created by Luka Mijić on 11.3.2017..
 */
public class Demonstration3 {

    public static void main(String[] args){


        try {
            Scanner sc = new Scanner(System.in);
            double[] dot = new double[3];
            double[][] elements = new double[3][3];
            for(int i=0;i<3;i++){
                System.out.println("Enter "+(i+1)+". vertex of triangle :");
                for(int j=0;j<3;j++){
                    elements[i][j] = sc.nextDouble();
                }
            }

            System.out.println("Enter vertex of a dot: ");
            for(int i=0;i<3;i++){
                dot[i] = sc.nextDouble();
            }


            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                    if(elements[i][j]!=0){
                        break;
                    }
                    if(j==2){
                        for(int k=0;k<3;k++){
                            elements[i][k] = 1;
                        }
                        dot[i] = 1;
                    }
                }
            }

            IMatrix matrix = new Matrix(3,3,elements, true);
            IVector vector = new Vector(dot);
            IMatrix mVector = new MatrixVectorView(vector,false);

            IMatrix result = matrix.nInvert().nMultiply(mVector);
            IVector vResutl = new VectorMatrixView(result);

            System.out.println("(x,y,z) = " + vResutl);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
