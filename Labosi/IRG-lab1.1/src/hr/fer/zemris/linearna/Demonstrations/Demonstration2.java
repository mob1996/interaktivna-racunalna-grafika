package hr.fer.zemris.linearna.Demonstrations;

import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;
import hr.fer.zemris.linearna.matrix.MatrixVectorView;
import hr.fer.zemris.linearna.vectors.IVector;
import hr.fer.zemris.linearna.vectors.Vector;
import hr.fer.zemris.linearna.vectors.VectorMatrixView;


import java.util.Scanner;

/**
 * Created by Luka Mijić on 11.3.2017..
 */
public class Demonstration2 {


    public static void main(String[] args){
        try{
            Scanner sc = new Scanner(System.in);
            double[] results = new double[3];
            int k=0;
            double[][] elements = new double[3][3];
            for(int i=0;i<3;i++){
                System.out.println("Enter "+(i+1)+". equation (format: xi+yj+zk=a):");
                for(int j=0;j<4;j++){
                    if(j<3){
                        elements[i][j] = sc.nextDouble();
                    } else {
                        results[k] = sc.nextDouble();
                        k++;
                    }
                }
            }

            IMatrix matrix = new Matrix(3,3,elements, true);
            IVector vector = new Vector(results);
            IMatrix mVector = new MatrixVectorView(vector,false);

            IMatrix result = matrix.nInvert().nMultiply(mVector);
            IVector vResutl = new VectorMatrixView(result);

            System.out.println("(x,y,z) = " + vResutl);

        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
//https://www.mathsisfun.com/algebra/systems-linear-equations-matrices.html