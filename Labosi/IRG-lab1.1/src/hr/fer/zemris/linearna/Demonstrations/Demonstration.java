package hr.fer.zemris.linearna.Demonstrations;

import hr.fer.zemris.linearna.matrix.IMatrix;
import hr.fer.zemris.linearna.matrix.Matrix;

import hr.fer.zemris.linearna.vectors.IVector;
import hr.fer.zemris.linearna.vectors.Vector;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Demonstration {

	public static void main(String[] args) {
		IVector a = Vector.parseSimple("1 0 0");
		IVector b = Vector.parseSimple("16 1 0");
		IVector c = Vector.parseSimple("14 17 0");
		
		IVector t = Vector.parseSimple("6 3 0");

		try {
			double pov = b.nSub(a).nVectorProduct(c.nSub(a)).norm()/2.0;
			double povA = b.nSub(t).nVectorProduct(c.nSub(t)).norm()/2.0;
			double povB = a.nSub(t).nVectorProduct(c.nSub(t)).norm()/2.0;
			double povC = a.nSub(t).nVectorProduct(b.nSub(t)).norm()/2.0;
			
			double t1 = povA/pov;
			double t2 = povB/pov;
			double t3 = povC/pov;

			System.out.println("("+t1+", "+t2+", "+t3+")");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try{
			IMatrix am = Matrix.parseSimple(" 3 5 | 2 10 ");
			IMatrix rm = Matrix.parseSimple(" 2 | 8 ");
			IMatrix vm = am.nInvert().nMultiply(rm);
			System.out.println("Rjesenje sustava je: \n"+vm);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			IMatrix m1 = Matrix.parseSimple("1 5 3 | 0 0 8 | 1 1 1");
			m1.nInvert();
			System.out.println(m1.nInvert().nMultiply(Matrix.parseSimple("3 | 4 | 1")));
		} catch (Exception e){
			e.printStackTrace();
		}

		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter vector dimension (2 or 3): ");
			int dimension = Integer.valueOf(reader.readLine());
			System.out.println(dimension);
			IVector n = null;
			IVector m = null;

			for(int i=0;i<2;i++){
				System.out.println(i+". Vector:");
				double[] elements = new double[dimension];
				for(int j=0;j<dimension;j++){
					elements[j] = Double.valueOf(reader.readLine());
				}
				if(i==0){
					n = new Vector(elements);
				} else {
					m = new Vector(elements);
				}

			}
			System.out.println(n);
			System.out.println(m);

			IVector k= n.nScalarMultiply(m.scalarProduct(n)).scalarMultiply(1/(n.norm()*n.norm()));
			IVector r = k.scalarMultiply(2).nSub(m);
			System.out.println("\n\n"+r);
		}catch (Exception e){
			e.printStackTrace();
		}

		
	}

}
