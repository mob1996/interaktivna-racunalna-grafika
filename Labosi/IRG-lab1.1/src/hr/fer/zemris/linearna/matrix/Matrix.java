package hr.fer.zemris.linearna.matrix;

import hr.fer.zemris.linearna.vectors.NotDefinedForGivenDimensions;

public class Matrix extends AbstractMatrix {

	private double[][] elements;
	private int rows;
	private int cols;
	
	public Matrix(int rows, int cols){
		this.rows = rows;
		this.cols = cols;
		elements = new double[rows][cols];
	}
	
	public Matrix(int rows, int cols, double[][] elements, boolean useGivenField){
		this.rows = rows;
		this.cols = cols;
		if(useGivenField){
			this.elements=elements;
		} else {
			this.elements = new double[rows][cols];
			for(int i=0;i<this.rows;i++){
				for(int j=0;j<this.cols;j++){
					this.elements[i][j] = elements[i][j];
				}
			}
		}
	}
	
	@Override
	public int getRowsCount() {
		return rows;
	}

	@Override
	public int getColsCount() {
		return cols;
	}

	@Override
	public double get(int row, int col) {
		if(row>=rows || col>=cols){
			throw new IndexOutOfBoundsException();
		}
		return elements[row][col];
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		if(row>=rows || col>=cols){
			throw new IndexOutOfBoundsException();
		}
		elements[row][col] = value;
		return this;
	}

	@Override
	public IMatrix copy() {
		IMatrix newMatrix = new Matrix(this.getRowsCount(), this.getColsCount(), this.toArray(), false);
		return newMatrix;
	}

	@Override
	public IMatrix newInstance(int nRow, int nCol) {
		return new Matrix(nRow, nCol);
	}
	
	public static Matrix parseSimple(String strMatrix) throws NotDefinedForGivenDimensions{
		String[] strRows = strMatrix.split("\\|");
		int rows = strRows.length;
		int cols = strRows[0].trim().split("\\s+").length;
		double[][] elements = new double[rows][cols];

		for(int i=0;i<rows;i++){
			strRows[i] = strRows[i].trim();
			if(strRows[i].split("\\s+").length!=cols){	
				throw new NotDefinedForGivenDimensions("Invalid matrix");
			}
			String[] strCols= strRows[i].split("\\s+");
			for(int j=0;j<cols;j++){
				elements[i][j] = Double.valueOf(strCols[j]);
			}
		}
		return new Matrix(rows, cols, elements, false);
		
	}

}
