package hr.fer.zemris.linearna.matrix;

import hr.fer.zemris.linearna.vectors.NotDefinedForGivenDimensions;

public class MatrixSubMatrixView extends AbstractMatrix {
	
	private IMatrix subMatrix;
	private int[] rowIndexes;
	private int[] colIndexes;
	
	
	public MatrixSubMatrixView(IMatrix original, int removeRow, int removeCol) {
		if(removeRow>original.getRowsCount()-1 || removeCol>original.getColsCount()-1){
			throw new IndexOutOfBoundsException("One of the indexes is out of bounds.");
		}

		int originalRows = original.getRowsCount();
		int originalCols = original.getColsCount();
		int newRows = originalRows-1;
		int newCols = originalCols-1;
		double[][] subElements = new double[newRows][newCols];
		int skipRows = 0;
		int skipCols = 0;
		for(int i=0;i<originalRows;i++){
			if(i==removeRow){
				skipRows++;
				continue;
			}
			for(int j=0;j<originalCols;j++){
				if(j==removeCol){
					skipCols++;
					continue;
				}
				subElements[i-skipRows][j-skipCols] = original.get(i,j);
			}
			skipCols=0;
		}

		subMatrix = new Matrix(newRows, newCols, subElements, false);

	}

	private MatrixSubMatrixView(IMatrix original, int[] rowIndexes, int[] colIndexes){
		
	}
	
	
	@Override
	public int getRowsCount() {
		return subMatrix.getRowsCount();
	}

	@Override
	public int getColsCount() {
		return subMatrix.getColsCount();
	}

	@Override
	public double get(int row, int col){
		return subMatrix.get(row,col);
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		return subMatrix.set(row,col,value);
	}

	@Override
	public IMatrix copy() throws NotDefinedForGivenDimensions {
		return subMatrix.copy();
	}

	@Override
	public IMatrix newInstance(int nRow, int nCol) {
		return subMatrix.newInstance(nRow, nCol);
	}
	


}
