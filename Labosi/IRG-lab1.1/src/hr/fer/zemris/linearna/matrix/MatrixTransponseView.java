package hr.fer.zemris.linearna.matrix;


public class MatrixTransponseView extends AbstractMatrix {

	IMatrix matrix;
	
	public MatrixTransponseView(IMatrix matrix) {
		this.matrix=matrix;
	}
	
	
	@Override
	public int getRowsCount() {
		return matrix.getColsCount();
	}

	@Override
	public int getColsCount() {
		return matrix.getRowsCount();
	}

	@Override
	public double get(int row, int col) {
		return matrix.get(col, row);
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		return matrix.set(col, row, value);
	}

	@Override
	public IMatrix copy() {
		int rows = this.getRowsCount();
		int cols = this.getColsCount();
		IMatrix newMatrix = new Matrix(rows, cols);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				newMatrix.set(i, j, this.get(i, j));
			}
		}
		return newMatrix;
		
	}

	@Override
	public IMatrix newInstance(int nRow, int nCol) {
		return new Matrix(nRow, nCol);
	}

}
