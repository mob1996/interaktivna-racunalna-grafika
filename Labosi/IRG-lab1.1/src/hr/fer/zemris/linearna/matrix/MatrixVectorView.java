package hr.fer.zemris.linearna.matrix;

import hr.fer.zemris.linearna.vectors.IVector;
import hr.fer.zemris.linearna.vectors.NotDefinedForGivenDimensions;

/**
 * Created by Luka Mijić on 11.3.2017..
 */
public class MatrixVectorView extends AbstractMatrix{

    private IVector vector;
    private boolean asRowMatrix;

    public MatrixVectorView(IVector original, boolean asRowMatrix){
        this.vector=original;
        this.asRowMatrix=asRowMatrix;
    }

    @Override
    public int getRowsCount() {
        if(asRowMatrix){
            return 1;
        } else {
            return vector.getDimension();
        }
    }

    @Override
    public int getColsCount() {
        if(asRowMatrix){
            return vector.getDimension();
        } else {
            return 1;
        }
    }

    @Override
    public double get(int row, int col) {
       if(asRowMatrix){
           return vector.get(col);
       } else {
           return vector.get(row);
       }
    }

    @Override
    public IMatrix set(int row, int col, double value) {
        try{
            if(asRowMatrix){
                vector.set(col,value);
            } else {
                vector.set(row, value);
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return this;
    }

    @Override
    public IMatrix copy() throws NotDefinedForGivenDimensions{
        IVector copy = vector.copy();
        return new MatrixVectorView(copy, asRowMatrix);
    }

    @Override
    public IMatrix newInstance(int nRow, int nCol) {
        return new Matrix(nRow, nCol);
    }
}
