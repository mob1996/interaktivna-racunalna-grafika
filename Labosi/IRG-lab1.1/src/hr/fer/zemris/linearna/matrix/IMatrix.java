package hr.fer.zemris.linearna.matrix;

import hr.fer.zemris.linearna.vectors.IVector;
import hr.fer.zemris.linearna.vectors.NotDefinedForGivenDimensions;

public interface IMatrix {
	public int getRowsCount();
	public int getColsCount();
	public double get(int row, int col);
	public IMatrix set(int row, int col, double value);
	public IMatrix copy() throws NotDefinedForGivenDimensions;
	public IMatrix newInstance(int nRow, int nCol);
	public IMatrix nTransponse(boolean liveView) throws NotDefinedForGivenDimensions;
	public IMatrix add(IMatrix other) throws NotDefinedForGivenDimensions;
	public IMatrix nAdd(IMatrix other) throws NotDefinedForGivenDimensions;
	public IMatrix sub(IMatrix other) throws NotDefinedForGivenDimensions;
	public IMatrix nSub(IMatrix other) throws NotDefinedForGivenDimensions;
	public IMatrix nMultiply(IMatrix other) throws NotDefinedForGivenDimensions;
	public IMatrix scalarMultiply(double scalar);
	public IMatrix nScalarMultiply(double scalar) throws  NotDefinedForGivenDimensions;
	public double determinant() throws NotDefinedForGivenDimensions;
	public IMatrix subMatrix(int nRows, int nCols, boolean liveView) throws NotDefinedForGivenDimensions;
	public IMatrix nInvert() throws NotDefinedForGivenDimensions;
	public double[][] toArray();
	public IVector toVector(boolean liveView) throws NotDefinedForGivenDimensions;
	public String toString(int precision);
}
