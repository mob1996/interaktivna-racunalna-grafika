package hr.fer.zemris.linearna.matrix;

/**
 * Created by Luka Mijić on 11.3.2017..
 */
public class IrregularMatrixException extends RuntimeException{

    public IrregularMatrixException(String str){
        super(str);
    }
}
