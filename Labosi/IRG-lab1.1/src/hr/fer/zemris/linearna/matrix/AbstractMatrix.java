package hr.fer.zemris.linearna.matrix;

import java.text.DecimalFormat;

import hr.fer.zemris.linearna.vectors.IVector;
import hr.fer.zemris.linearna.vectors.NotDefinedForGivenDimensions;
import hr.fer.zemris.linearna.vectors.Vector;
import hr.fer.zemris.linearna.vectors.VectorMatrixView;

public abstract class AbstractMatrix implements IMatrix {

	@Override
	public abstract int getRowsCount();

	@Override
	public abstract int getColsCount();

	@Override
	public abstract double get(int row, int col);

	@Override
	public abstract IMatrix set(int row, int col, double value);

	@Override
	public abstract IMatrix copy() throws NotDefinedForGivenDimensions;

	@Override
	public abstract IMatrix newInstance(int nRow, int nCol);

	@Override
	public IMatrix nTransponse(boolean liveView) throws NotDefinedForGivenDimensions{
		IMatrix newMatrix;
		if(liveView){
			newMatrix = new MatrixTransponseView(this);
		} else {
			newMatrix = new MatrixTransponseView(this.copy());
		}
		return newMatrix;
	}

	@Override
	public IMatrix add(IMatrix other) throws NotDefinedForGivenDimensions {
		if(this.getRowsCount()!=other.getRowsCount() || this.getColsCount()!=other.getColsCount()){
			throw new NotDefinedForGivenDimensions("Dimensions must be the same");
		}
		int rows = this.getRowsCount();
		int cols = this.getColsCount();
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				this.set(i, j, this.get(i, j)+other.get(i, j));
			}
		}
		return this;
	}

	@Override
	public IMatrix nAdd(IMatrix other) throws NotDefinedForGivenDimensions {
		return this.copy().add(other);
	}

	@Override
	public IMatrix sub(IMatrix other) throws NotDefinedForGivenDimensions {
		if(this.getRowsCount()!=other.getRowsCount() || this.getColsCount()!=other.getColsCount()){
			throw new NotDefinedForGivenDimensions("Dimensions must be the same");
		}
		int rows = this.getRowsCount();
		int cols = this.getColsCount();
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;i++){
				this.set(i, j, this.get(i, j)-other.get(i, j));
			}
		}
		return this;
	}

	@Override
	public IMatrix nSub(IMatrix other) throws NotDefinedForGivenDimensions {
		return this.copy().sub(other);
	}

	@Override
	public IMatrix nMultiply(IMatrix other) throws NotDefinedForGivenDimensions {
		if(this.getColsCount()!=this.getRowsCount()){
			throw new NotDefinedForGivenDimensions("Number of colums from first matrix must be the same as number of rows of second matrix.");
		}
		int rows = this.getRowsCount();
		int cols = other.getColsCount();
		IMatrix newMatrix = this.newInstance(rows, cols);
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				newMatrix.set(i, j, calculateValue(i,j, this, other));
			}
		}
		return newMatrix;
	}
	
	@Override
	public IMatrix scalarMultiply(double scalar){
		int rows = this.getRowsCount();
		int cols = this.getColsCount();
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				this.set(i, j, this.get(i, j)*scalar);
			}
		}
		return this;
	}
	
	@Override 
	public IMatrix nScalarMultiply(double scalar) throws NotDefinedForGivenDimensions{
		return this.copy().scalarMultiply(scalar);
	}

	//jednak broj redaka i stupaca obavezan
	@Override
	public double determinant() throws NotDefinedForGivenDimensions {
		if(this.getRowsCount()!=this.getColsCount()){
			throw new NotDefinedForGivenDimensions("Number of columns must be the same as number of rows.");
		}
		int dimension=this.getRowsCount();
		if(dimension==2){
			return this.get(0,0)*this.get(1,1)-this.get(1,0)*this.get(0,1);
		} else if(dimension==1){
			return this.get(0,0);
		} else {
			double detM=0;
			int i=0;
			for(int j=0;j<dimension;j++){
				detM+=Math.pow((-1), i+j)*this.get(i,j)*this.subMatrix(i,j,false).determinant();
			}
			return detM;
		}
	}

	@Override
	public IMatrix subMatrix(int nRows, int nCols, boolean liveView) throws NotDefinedForGivenDimensions {
		if(liveView==true){
			return new MatrixSubMatrixView(this, nRows, nCols);
		} else {
			return new MatrixSubMatrixView(this.copy(), nRows, nCols);
		}
	}

	@Override
	public IMatrix nInvert() throws NotDefinedForGivenDimensions {
		if(this.getRowsCount()!=this.getColsCount()){
			throw new NotDefinedForGivenDimensions("Number of columns must be the same as number of rows.");
		}
		int dimension = this.getRowsCount();
		double detM = this.determinant();
		if(detM==0){
			throw new IrregularMatrixException("Matrix must be regular (determinant of original must not be zero).");
		}
		double[][] elements = new double[dimension][dimension];
		for(int i=0;i<dimension;i++){
			for(int j=0;j<dimension;j++){
				elements[i][j] = Math.pow(-1, i+j)*this.subMatrix(i,j, false).determinant();
			}
		}
		return new Matrix(dimension, dimension, elements, false).scalarMultiply(1/detM).nTransponse(true);
	}

	@Override
	public double[][] toArray() {
		int rows = this.getRowsCount();
		int cols = this.getColsCount();
		double[][] matrix = new double[rows][cols];
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				matrix[i][j] = this.get(i, j);
			}
		}
		return matrix;
	}

	@Override
	public IVector toVector(boolean liveView) throws NotDefinedForGivenDimensions {
		if(this.getRowsCount()!=1 && this.getColsCount()!=1){
			throw new NotDefinedForGivenDimensions("Dimensions must be 1xm or mx1");
		}
		int dimension;
		if(this.getRowsCount()==1){
			dimension=this.getColsCount();
		} else {
			dimension=this.getRowsCount();
		}
		if(liveView==false){
			double[] elements = new double[dimension];
			int counter = 0;
			for(int i =0;i<this.getRowsCount();i++){
				for(int j=0;j<this.getColsCount();j++){
					elements[counter] = this.get(i,j);
					counter++;
				}
			}
			return new Vector(elements);
		} else {
			return new VectorMatrixView(this);
		}

	}
	
	private double calculateValue(int row, int col, IMatrix first, IMatrix second){
		double value = 0;
		int m=first.getRowsCount();
		for(int i=0;i<m;i++){
			value+=(first.get(row, i)*second.get(i, col));
		}
		return value;
	}
	
	@Override
	public String toString(){
		return this.toString(3);
	}
	
	public String toString(int precision){
		int rows = this.getRowsCount();
		int cols = this.getColsCount();
		StringBuilder sb= new StringBuilder();
		String sPrecision= "";
		for(int i=0;i<precision;i++){
			sPrecision+="0";
		}
		
		for(int i=0;i<rows;i++){
			sb.append("[");
			for(int j=0;j<cols;j++){
				String strValue = new DecimalFormat("#0."+sPrecision).format(this.get(i,j));
				strValue = strValue.replace(",", ".");
				sb.append(strValue);
				if(j<cols-1){
					sb.append(", ");
				}
			}
			sb.append("]\n");
		}
		
		return sb.toString();
	}

}
