package hr.fer.zemris.linearna.vectors;

public class Vector extends AbstractVector {
	
	private double[] elements;
	private int dimension;
	private boolean readOnly;
	
	public Vector(double[] values){
		dimension = values.length;
		readOnly = false;
		elements = new double[dimension];
		for(int i=0;i<dimension;i++){
			elements[i] = values[i];
		}
	}
	
	public Vector(boolean readOnly, boolean useGivenField, double[] values){
		this.readOnly = readOnly;
		dimension = values.length;
		if(useGivenField == true){
			elements = values;
		} else {
			elements = new double[dimension];
			for(int i=0;i<dimension;i++){
				elements[i] = values[i];
			}
		}
	}

	@Override
	public double get(int elemIndex) {
		if(elemIndex>elements.length-1){
			throw new IndexOutOfBoundsException();
		}
		return elements[elemIndex];
	}

	@Override
	public IVector set(int elemIndex, double elemValue) throws VectorIsNotWritableException {
		if(readOnly == true){
			throw new VectorIsNotWritableException();
		} else if(elemIndex>elements.length){
			throw new IndexOutOfBoundsException();
		} 
		elements[elemIndex] = elemValue;
		return this;
	}

	@Override
	public int getDimension() {
		return dimension;
	}

	@Override
	public IVector copy() {
		Vector copy = new Vector(readOnly, false, elements);
		return copy;
	}

	@Override
	public IVector newInstance(int newI) {
		double[] values = new double[newI];
		return new Vector(values);
	}
	
	public static Vector parseSimple(String str){
		String[] strValues = str.split("\\s+");
		double[] values = new double[strValues.length];
		for(int i=0;i<values.length;i++){
			values[i] = Double.valueOf(strValues[i]);
		}
		return new Vector(values);	
	}

}
