package hr.fer.zemris.linearna.vectors;

import java.text.DecimalFormat;

public abstract class AbstractVector implements IVector {

	@Override
	public abstract double get(int elemIndex);

	@Override
	public abstract IVector set(int elemIndex, double elemValue) throws VectorIsNotWritableException;

	@Override
	public abstract int getDimension();

	@Override
	public abstract IVector copy() throws NotDefinedForGivenDimensions;
	
	@Override
	public abstract IVector newInstance(int newI);

	@Override
	public IVector copyPart(int n) throws VectorIsNotWritableException {
		IVector newVector = this.newInstance(n);
		int thisDim = this.getDimension();
		for(int i=0;i<n;i++){
			if(i<thisDim){
				newVector.set(i, this.get(i));
			} else {
				newVector.set(i, 0);
			}
		}
		return newVector;
	}

	

	@Override
	public IVector add(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException {
		if(this.getDimension()!=other.getDimension()){
			throw new IncompatibleOperandException();
		}
		int dim=this.getDimension();
		for(int i=0;i<dim;i++){
			this.set(i, this.get(i)+other.get(i));
		}
		return this;
	}

	@Override
	public IVector nAdd(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException, NotDefinedForGivenDimensions {
		return this.copy().add(other);
	}

	@Override
	public IVector sub(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException {
		if(this.getDimension()!=other.getDimension()){
			throw new IncompatibleOperandException();
		}
		int dim=this.getDimension();
		for(int i=0;i<dim;i++){
			this.set(i, this.get(i)-other.get(i));
		}
		return this;
	}

	@Override
	public IVector nSub(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException, NotDefinedForGivenDimensions {
		return this.copy().sub(other);
	}

	@Override
	public IVector scalarMultiply(double scalar) throws VectorIsNotWritableException {
		int dim=this.getDimension();
		for(int i=0;i<dim;i++){
			this.set(i, scalar*this.get(i));
		}
		return this;
	}

	@Override
	public IVector nScalarMultiply(double scalar) throws VectorIsNotWritableException, NotDefinedForGivenDimensions {
		return this.copy().scalarMultiply(scalar);
	}
	
	//prakticki duljina
	@Override
	public double norm() {
		int dim=this.getDimension();
		double underRoot=0;
		for(int i=0;i<dim;i++){
			underRoot+=Math.pow(this.get(i), 2);
		}
		return Math.sqrt(underRoot);
	}

	//dobivanje jedini�nog vektora, tj. ima isti smjer samo mu je duljina = 1
	//v1=v/v.norm()
	@Override
	public IVector normalize() throws VectorIsNotWritableException {
		double norm=this.norm();
		int dim=this.getDimension();
		for(int i=0;i<dim;i++){
			this.set(i, this.get(i)/norm);
		}
		return this;
	}

	@Override
	public IVector nNormalize() throws VectorIsNotWritableException, NotDefinedForGivenDimensions {
		return this.copy().normalize();
	}

	//vrijednost cos(l), l kut izmedju 2 vektora 
	//cos(l)= (a*b)/(|a||b|)
	@Override
	public double cosine(IVector other) throws IncompatibleOperandException {
		if(this.getDimension()!=other.getDimension()){
			throw new IncompatibleOperandException();
		}
		double scalarProduct = this.scalarProduct(other);
		double addNorms = this.norm()+other.norm();
		double cosine=scalarProduct/addNorms;
		return cosine;
	}

	@Override
	public double scalarProduct(IVector other) throws IncompatibleOperandException {
		if(this.getDimension()!=other.getDimension()){
			throw new IncompatibleOperandException();
		}
		int dim=this.getDimension();
		double scalarProduct=0;
		for(int i=0;i<dim;i++){
			scalarProduct+=(this.get(i)*other.get(i));
		}
		return scalarProduct;
	}

	//vektorski produkt se dogadja u 3D prostoru
	// x=axb
	// x=(a2*b3-a3b2)i+(a3b1-a1b3)j+(a1b2-a2b1)k
	@Override
	public IVector nVectorProduct(IVector other) throws NotDefinedForGivenDimensions, VectorIsNotWritableException {
		int definedDim=3;
		if(this.getDimension()!=definedDim || other.getDimension()!=definedDim){
			throw new NotDefinedForGivenDimensions("Vector dimensions must be the same.");
		}
		IVector vectorProduct = this.newInstance(definedDim);
		double[] directions = new double[3];
		directions[0] = this.get(1)*other.get(2)-this.get(2)*other.get(1);
		directions[1] = this.get(2)*other.get(0)-this.get(0)*other.get(2);
		directions[2] = this.get(0)*other.get(1)-this.get(1)*other.get(0);
		
		for(int i=0;i<definedDim;i++){
			vectorProduct.set(i, directions[i]);
		}
		
		return vectorProduct;
	}

	@Override
	public IVector nFromHomogeneus() throws VectorIsNotWritableException {
		int thisDim = this.getDimension();
		IVector newVector = this.newInstance(thisDim-1);
		double homogenCordinate = this.get(thisDim-1);
		for(int i=0;i<newVector.getDimension();i++){
			this.set(i, this.get(i)/homogenCordinate);
		}
		return newVector;
	}

	@Override
	public double[] toArray() {
		int size = this.getDimension();
		double[] vectorArray = new double[size];
		for(int i=0;i<size;i++){
			vectorArray[i] = this.get(i);
		}
		return vectorArray;
	}
	
	@Override
	public String toString(){
		return this.toString(3);
	}
	
	
	public String toString(int precision){
		int dim = this.getDimension();
		String sPrecision= "";
		for(int i=0;i<precision;i++){
			sPrecision+="0";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(int i=0;i<dim;i++){
			String strValue = new DecimalFormat("#0."+sPrecision).format(this.get(i));
			strValue = strValue.replace(",", ".");
			sb.append(strValue);
			if(i<dim-1){
				sb.append(", ");
			}
		}
		sb.append(")\n");
		return sb.toString();
	}
	
}
