package hr.fer.zemris.linearna.vectors;

import hr.fer.zemris.linearna.matrix.IMatrix;



/**
 * Created by Luka Mijić on 11.3.2017..
 */

public class VectorMatrixView extends AbstractVector {

    private IMatrix matrix;
    private int dimension;
    private boolean rowMatrix;

    public  VectorMatrixView(IMatrix original) throws  NotDefinedForGivenDimensions{
        if(original.getRowsCount()!=1 && original.getColsCount()!=1){
            throw new NotDefinedForGivenDimensions("Dimensions of matrix must be 1xm or mx1!");
        }
        this.matrix=original;
        if(original.getRowsCount()==1){
            dimension=original.getColsCount();
            rowMatrix=true;
        } else {
            dimension = original.getRowsCount();
            rowMatrix = false;
        }
    }

    @Override
    public double get(int elemIndex) {
        if(rowMatrix){
            return matrix.get(0,elemIndex);
        } else {
            return  matrix.get(elemIndex,0);
        }
    }

    @Override
    public IVector set(int elemIndex, double elemValue) throws VectorIsNotWritableException {
        if(rowMatrix){
            matrix.set(0, elemIndex, elemValue);
        } else {
            matrix.set(elemIndex,0,elemValue);
        }
        return this;
    }

    @Override
    public int getDimension() {
        return dimension;
    }

    @Override
    public IVector copy() throws NotDefinedForGivenDimensions {
        IMatrix copy = matrix.copy();
        return new VectorMatrixView(copy);
    }

    @Override
    public IVector newInstance(int newI) {
        double[] values = new double[newI];
        return new Vector(values);
    }
}
