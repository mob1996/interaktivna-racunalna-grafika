package hr.fer.zemris.linearna.vectors;

public interface IVector {
	public double get(int elemIndex);
	public IVector set(int elemIndex, double elemValue) throws VectorIsNotWritableException;
	public int getDimension();
	public IVector copy() throws NotDefinedForGivenDimensions;
	public IVector copyPart(int n) throws VectorIsNotWritableException;
	public IVector newInstance(int newI);
	public IVector add(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException;
	public IVector nAdd(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException, NotDefinedForGivenDimensions;
	public IVector sub(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException;
	public IVector nSub(IVector other) throws IncompatibleOperandException, VectorIsNotWritableException, NotDefinedForGivenDimensions;
	public IVector scalarMultiply(double scalar) throws VectorIsNotWritableException;
	public IVector nScalarMultiply(double scalar) throws VectorIsNotWritableException, NotDefinedForGivenDimensions;
	public double norm();
	public IVector normalize() throws VectorIsNotWritableException, NotDefinedForGivenDimensions;
	public IVector nNormalize() throws VectorIsNotWritableException, NotDefinedForGivenDimensions;
	public double cosine(IVector other) throws IncompatibleOperandException;
	public double scalarProduct(IVector other) throws IncompatibleOperandException;
    public IVector nVectorProduct(IVector other) throws NotDefinedForGivenDimensions, VectorIsNotWritableException;
	public IVector nFromHomogeneus() throws VectorIsNotWritableException;
	//public IMatrix toRowMatrix(boolean liveView);
	//public IMatrix toColumnMatrix(boolean liveView);
	public double[] toArray();
	public String toString(int precision);

}
