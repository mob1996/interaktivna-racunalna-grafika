package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by Luka on 4.5.2017..
 */
public class MyRunnable implements Runnable{

    private static ArrayList<Vertex3Df> vertices = new ArrayList<>();
    public static ArrayList<TrianglePolygon> polygons = new ArrayList<>();

    public static Vertex3Df O = new Vertex3Df(50,50,50);
    public static Vertex3Df G = new Vertex3Df(0,0,0);

    @Override
    public void run(){
        readFile();
        initGUI();
    }

    private void initGUI(){
        JFrame frame = new JFrame("Exercise 3.1.");
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLCanvas glCanvas =  new GLCanvas(glCapabilities);


        glCanvas.addGLEventListener(new MyGLEventListener());
        frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
        frame.setSize(800,800);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });
    }



    public void readFile(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("D:\\Faks\\Interaktivna računalna grafika\\Labosi\\IRG-lab3.1\\kocka.obj"));
            String line;
            while((line=reader.readLine())!=null){
                if(!(line.startsWith("v") || line.startsWith("f"))){
                    continue;
                }
                String[] tokens = line.split("\\s+");
                if(tokens[0].equals("v")){
                    processVertex(tokens);
                } else if(tokens[0].equals("f")){
                    processPolygon(tokens);
                }

            }
            reader.close();
        } catch (Exception e){
            System.out.println("Greska pri ocitavanju objekta");
            e.printStackTrace();
        }

    }

    private void processVertex(String[] tokens){
        float x = Float.valueOf(tokens[1]);
        float y = Float.valueOf(tokens[2]);
        float z = Float.valueOf(tokens[3]);
        Vertex3Df vertex = new Vertex3Df(x,y,z);
        vertices.add(vertex);
    }

    private void processPolygon(String[] tokens){
        int indexOfFirst = Integer.valueOf(tokens[1].split("\\.")[0])-1;
        int indexOfSecond = Integer.valueOf(tokens[2].split("\\.")[0])-1;
        int indexOfThird =  Integer.valueOf(tokens[3].split("\\.")[0])-1;
        TrianglePolygon trianglePolygon = new TrianglePolygon(vertices.get(indexOfFirst), vertices.get(indexOfSecond), vertices.get(indexOfThird));
        polygons.add(trianglePolygon);
    }
}


