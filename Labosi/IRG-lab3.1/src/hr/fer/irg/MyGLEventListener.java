package hr.fer.irg;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;



/**
 * Created by Luka on 4.5.2017..
 */
public class MyGLEventListener implements GLEventListener {

    private GLU glu = new GLU();


    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClearColor(0,0,0,0);
        gl2.glClearDepth(1.0f);
        gl2.glEnable(GL.GL_DEPTH_TEST);
        gl2.glDepthFunc(GL.GL_LEQUAL);
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClear (GL2.GL_COLOR_BUFFER_BIT |  GL2.GL_DEPTH_BUFFER_BIT );
        gl2.glLoadIdentity();
        //double[] translate = {1,0,0,0,0,1,0,0,0,0,1,0,0.2f,0.1f,4.0f,1};
        //gl2.glMultMatrixd(translate,0);
        double[] matrix = {-0.7071, -0.6396, -0.3015, 0,
                            0.7071, -0.6396, -0.3015, 0,
                            0, 0.4264, 0.9045, 0,
                            0, 0, -3.316624, 1};
        modelViewMatrixMulti(gl2);
        gl2.glScalef(0.5f,0.5f,0.5f);
        gl2.glTranslatef(-0.5f,-0.5f,-0.5f);
        //gl2.glMultMatrixd(matrix,0);
        for(TrianglePolygon tp:MyRunnable.polygons){
            drawPolygon(tp, gl2);
        }

    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glViewport(0,0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        //glu.gluOrtho2D(-5, 5, -5, 5);
        gl2.glMultMatrixd(perspectiveMatrix(),0);
        double[] matrix = new double[16];
        gl2.glGetDoublev(GL2.GL_PROJECTION_MATRIX, matrix, 0);
        printMatrix4x4(matrix);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
    }


    private double[] perspectiveMatrix(){
        Vertex3Df O = MyRunnable.O;
        Vertex3Df G = MyRunnable.G;
        double x_= pow(O.getX()-G.getX(),2);
        double y_= pow(O.getY()-G.getY(),2);
        double z_= pow(O.getZ()-G.getZ(),2);
        double H = sqrt(x_+y_+z_);

        double[] matrixValues = {1,0,0,0,
                                0,1,0,0,
                                0,0,0,1/H,
                                0,0,0,0};

        return matrixValues;
    }

    private double[] modelViewMatrixMulti(GL2 gl2){
        double[] matrix =  new double[16];

        double Gx = MyRunnable.G.getX();
        double Gy = MyRunnable.G.getY();
        double Gz = MyRunnable.G.getZ();
        double Ox = MyRunnable.O.getX();
        double Oy = MyRunnable.O.getY();
        double Oz = MyRunnable.O.getZ();

        double[] translate = {1, 0, 0, 0,
                              0, 1, 0, 0,
                              0, 0, 1, 0,
                              -Ox, -Oy, -Oz, 1};

        Gx=Gx-Ox;
        Gy=Gy-Oy;
        Gz=Gz-Oz;

        double sinL = Gy/sqrt(pow(Gx,2)+pow(Gy,2));
        double cosL = Gx/sqrt(pow(Gx,2)+pow(Gy,2));
        Gx=sqrt(pow(Gx,2)+pow(Gy,2));
        Gy=0;
        Gz=Gz;

        double[] rotateZ = {cosL, -sinL, 0, 0,
                            sinL, cosL, 0, 0,
                            0, 0, 1, 0,
                            0, 0, 0, 1};

        double sinB = Gx/sqrt(pow(Gx,2)+pow(Gz,2));
        double cosB = Gz/sqrt(pow(Gx,2)+pow(Gz,2));
        double[] rotateY = {cosB, 0, sinB, 0,
                            0, 1, 0, 0,
                            -sinB, 0, cosB, 0,
                            0, 0, 0, 1};

        double[] T4 = {0, -1, 0, 0,
                       1, 0, 0, 0,
                       0, 0, 1, 0,
                       0, 0, 0, 1};
        double[] T5 = {-1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1};

        gl2.glMultMatrixd(T5,0);
        gl2.glMultMatrixd(T4,0);
        gl2.glMultMatrixd(rotateY, 0);
        gl2.glMultMatrixd(rotateZ, 0);
        gl2.glMultMatrixd(translate, 0);

        gl2.glGetDoublev(GL2.GL_MODELVIEW_MATRIX, matrix, 0);
        printMatrix4x4(matrix);




        return matrix;
    }

    private void printMatrix1x4(double[][] matrix){
        for(int i=0;i<4;i++){
            System.out.print(matrix[0][i]+"   ");
        }
        System.out.println();
    }

    private void printMatrix4x4(double[] matrix){
        for(int i=0;i<16;i++){
            if(i%4==0){
                System.out.println();
            }
            System.out.print(matrix[i]+"  ");
        }
        System.out.println();
    }

    private void drawPolygon(TrianglePolygon tp, GL2 gl2){
        gl2.glBegin(GL2.GL_LINE_LOOP);
        gl2.glColor3f(1, 1, 0);
        gl2.glVertex3f(tp.getFirst().getX(),
                tp.getFirst().getY(),
                tp.getFirst().getZ());
        gl2.glVertex3f(tp.getSecond().getX(),
                tp.getSecond().getY(),
                tp.getSecond().getZ());
        gl2.glVertex3f(tp.getThird().getX(),
                tp.getThird().getY(),
                tp.getThird().getZ());
        gl2.glEnd();

    }
}

