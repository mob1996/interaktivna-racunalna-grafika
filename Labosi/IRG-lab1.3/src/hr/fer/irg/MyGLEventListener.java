package hr.fer.irg;


import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;

/**
 * Created by Luka Mijić on 10.3.2017..
 */
public class MyGLEventListener implements GLEventListener{


    GLCanvas glCanvas;

    public MyGLEventListener(GLCanvas glCanvas){
        this.glCanvas = glCanvas;
    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();

        gl2.glClearColor(1, 1, 1, 1);
        gl2.glLoadIdentity();
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT);

        if(MyRunnable.isCutCanvas()){
            MyRunnable.setLimitXLeft(glCanvas.getSurfaceWidth()*1/4);
            MyRunnable.setLimitXRight(glCanvas.getSurfaceWidth()*3/4);
            MyRunnable.setLimitYDown(glCanvas.getSurfaceHeight()*1/4);
            MyRunnable.setLimitYUp(glCanvas.getSurfaceHeight()*3/4);
            drawFrame(gl2);
            for(Line l:MyRunnable.getLines()){
                Bresenham.cutScreen(gl2, l.getStart().getX(), l.getStart().getY(), l.getEnd().getX(), l.getEnd().getY());
            }
            System.out.print("\n\n");
        } else {
            MyRunnable.setLimitXLeft(0);
            MyRunnable.setLimitXRight(glCanvas.getSurfaceWidth());
            MyRunnable.setLimitYDown(0);
            MyRunnable.setLimitYUp(glCanvas.getSurfaceHeight());
            for(Line l:MyRunnable.getLines()){
                Bresenham.draw(gl2, l.getStart().getX(), l.getStart().getY(), l.getEnd().getX(), l.getEnd().getY());
            }
        }



        if(MyRunnable.isControl()){
            gl2.glBegin(GL2.GL_LINES);
            gl2.glColor3f(1,0,0);
            for(Line l:MyRunnable.getLines()){
                int translationX;
                int translationY;
                double incline = (l.getEnd().getY()-l.getStart().getY())/(double)(l.getEnd().getX()-l.getStart().getX());
                if(l.getStart().getX()<=l.getEnd().getX()){
                    if(Math.abs(incline)<0.01){
                        translationX=0;
                        translationY=-4;
                    } else if(Math.abs(incline)>100){
                        translationX=4;
                        translationY=0;
                    } else {
                        translationX=4;
                        translationY=-4;
                    }
                } else {
                    if(Math.abs(incline)<0.01){
                        translationX=0;
                        translationY=+4;
                    } else if(Math.abs(incline)>100){
                        translationX=4;
                        translationY=0;
                    } else {
                        translationX=4;
                        translationY=4;
                    }
                }
                gl2.glVertex2i(l.getStart().getX()+translationX, l.getStart().getY()+translationY);
                gl2.glVertex2i(l.getEnd().getX()+translationX, l.getEnd().getY()+translationY);
            }
            gl2.glEnd();
        }



    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();

        gl2.glDisable(GL2.GL_DEPTH_TEST);
        gl2.glViewport(0, 0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        gl2.glOrtho(0, width-1, 0, height-1, 0, 1);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
    }

    private void drawFrame(GL2 gl2){

        gl2.glBegin(GL2.GL_LINE_STRIP);
        gl2.glColor3f(0,1,0);
        gl2.glVertex2i(MyRunnable.getLimitXLeft(), MyRunnable.getLimitYDown());
        gl2.glVertex2i(MyRunnable.getLimitXLeft(), MyRunnable.getLimitYUp());
        gl2.glVertex2i(MyRunnable.getLimitXRight(), MyRunnable.getLimitYUp());
        gl2.glVertex2i(MyRunnable.getLimitXRight(), MyRunnable.getLimitYDown());
        gl2.glVertex2i(MyRunnable.getLimitXLeft(), MyRunnable.getLimitYDown());

        gl2.glEnd();

    }
}
