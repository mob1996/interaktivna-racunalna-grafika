package hr.fer.irg;

import com.jogamp.opengl.GL2;

/**
 * Created by Luka Mijić on 10.3.2017..
 */
public class Bresenham {


    public static void cutScreen(GL2 gl2, int xStart, int yStart, int xEnd, int yEnd){
        int startCode = calculateAreaCode(xStart, yStart);
        int endCode = calculateAreaCode(xEnd, yEnd);

        if((startCode & endCode)!= 0){
            return;
        } else {
            double incline = (yEnd-yStart)/(double) (xEnd-xStart);
            double l = -incline*xStart+yStart;
            Point newStart = newPoint(startCode, xStart, yStart, incline, l);
            Point newEnd = newPoint(endCode, xEnd, yEnd, incline, l);
            draw(gl2, newStart.getX(), newStart.getY(), newEnd.getX(), newEnd.getY());
        }
    }

    public static void draw(GL2 gl2, int xStart, int yStart, int xEnd, int yEnd) {
        if(xStart<=xEnd){
            if(yStart<=yEnd)
                draw_2(gl2,xStart,yStart,xEnd,yEnd);
            else
                draw_3(gl2,xStart,yStart,xEnd,yEnd);
        } else {
            if(yStart>=yEnd)
                draw_2(gl2,xEnd, yEnd, xStart, yStart);
            else
                draw_3(gl2,xEnd, yEnd, xStart, yStart);
        }
    }


    public static void draw_2(GL2 gl2, int xStart, int yStart, int xEnd, int yEnd) {
        int xHelper, yCurrent, correction;
        int incline, yf;

        gl2.glBegin(GL2.GL_POINTS);
        gl2.glColor3f(0,0,0);

        if(yEnd-yStart<=xEnd-xStart){
            incline = 2*(yEnd-yStart);
            yCurrent=yStart;
            yf=-(xEnd-xStart);
            correction=-2*(xEnd-xStart);
            for(xHelper = xStart; xHelper<=xEnd; xHelper++){
                gl2.glVertex2i(xHelper, yCurrent);
                yf+=incline;
                if(yf>=0){
                    yf+=correction;
                    yCurrent++;
                }
            }
        } else {
            xHelper=xEnd;
            xEnd=yEnd;
            yEnd=xHelper;

            xHelper=xStart;
            xStart=yStart;
            yStart=xHelper;

            incline=2*(yEnd-yStart);
            yCurrent=yStart;
            yf=-(xEnd-xStart);
            correction=-2*(xEnd-xStart);

            for(xHelper=xStart;xHelper<=xEnd;xHelper++){
                    gl2.glVertex2i(yCurrent,xHelper);
                yf+=incline;
                if(yf>=0){
                    yf+=correction;
                    yCurrent++;                }
            }
        }
        gl2.glEnd();
    }

    public static void draw_3(GL2 gl2, int xStart, int yStart, int xEnd, int yEnd) {
        int xHelper, yCurrent, correction;
        int incline, yf;

        gl2.glBegin(GL2.GL_POINTS);
        gl2.glColor3f(0,0,0);

        if(-(yEnd-yStart)<=xEnd-xStart) {
            incline = 2 * (yEnd - yStart);
            yCurrent = yStart;
            yf = (xEnd - xStart);
            correction = 2 * (xEnd - xStart);
            for (xHelper = xStart; xHelper <= xEnd; xHelper++){
                    gl2.glVertex2i(xHelper, yCurrent);
                yf += incline;
                if (yf <= 0) {
                    yf += correction;
                    yCurrent--;
                }
            }
        } else {
            xHelper=xEnd;
            xEnd=yStart;
            yStart=xHelper;

            xHelper=xStart;
            xStart=yEnd;
            yEnd=xHelper;

            incline=2*(yEnd-yStart);
            yCurrent=yStart;
            yf=xEnd-xStart;
            correction=2*(xEnd-xStart);

            for(xHelper=xStart;xHelper<=xEnd;xHelper++){
                gl2.glVertex2i(yCurrent,xHelper);
                yf+=incline;
                if(yf<=0){
                    yf+=correction;
                    yCurrent--;
                }
            }
        }

        gl2.glEnd();
    }

    private static int calculateAreaCode(int x, int y){
        int code = 0x0000;
        if(y>MyRunnable.getLimitYUp()){
            int mask = 0x0001 << 3;
            code = code | mask;
        }
        if (y<MyRunnable.getLimitYDown()){
            int mask = 0x0001 << 2;
            code = code | mask;
        }
        if (x>MyRunnable.getLimitXRight()){
            int mask = 0x0001 << 1;
            code = code | mask;
        }
        if (x<MyRunnable.getLimitXLeft()) {
            int mask = 0x0001;
            code = code | mask;
        }
        return code;
    }

    private static Point newPoint(int code, int x, int y, double incline, double l){
        while(code!=0){
            if((code & 0x0001)!=0){  //x less than xLeft, fix
                x=MyRunnable.getLimitXLeft();
                y=(int)(incline*x+l);
            }
            if((code & 0x0001<<1)!=0) {
                x = MyRunnable.getLimitXRight();
                y = (int) (incline * x + l);
            }
            if((code & 0x0001<<2)!=0){
                y=MyRunnable.getLimitYDown();
                x=(int)((y-l)/incline);
            }
            if((code & 0x0001<<3)!=0) {
                y = MyRunnable.getLimitYUp();
                x = (int) ((y - l) / incline);
            }
            code = calculateAreaCode(x,y);
        }


        Point p= new Point(x,y);
        return p;
    }


}
