package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by Luka Mijić on 10.3.2017..
 */
public class MyRunnable implements Runnable {
    private static ArrayList<Point> points = new ArrayList<>();
    private static ArrayList<Line> lines = new ArrayList<>();

    private static boolean control = false;
    private static boolean cutCanvas = false;

    private static int limitXLeft;
    private static int limitXRight;
    private static int limitYUp;
    private static int limitYDown;


    @Override
    public void run() {
        JFrame frame = new JFrame("Vjezba_3");
        JPanel north = new JPanel(new FlowLayout());
        JLabel controLabel = new JLabel();
        JLabel cutCanvasLabel = new JLabel();

        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLCanvas glCanvas = new GLCanvas(glCapabilities);

        glCanvas.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                Point p = new Point(e.getX(),glCanvas.getSurfaceHeight()-e.getY());
                points.add(p);
                if(points.size()%2==0){
                    Line l = new Line(points.get(0), points.get(1));
                    lines.add(l);
                    points.clear();
                    glCanvas.display();
                }
            }

        });



        glCanvas.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_O){
                    if(cutCanvas)
                        cutCanvas = false;
                    else
                        cutCanvas = true;
                    setLabel(cutCanvasLabel, "Cut Screen(O)", cutCanvas);
                    glCanvas.display();
                } else if(e.getKeyCode()==KeyEvent.VK_K){
                    if(control)
                        control = false;
                    else
                        control = true;
                    setLabel(controLabel, "Control(K)", control);
                    glCanvas.display();
                }
                if(e.getKeyCode()==KeyEvent.VK_C){
                    lines.clear();
                    points.clear();
                    glCanvas.display();
                }
            }
        });

        glCanvas.addGLEventListener(new MyGLEventListener(glCanvas));

        setLabel(controLabel, "Control (K)", control);
        setLabel(cutCanvasLabel, "Cut Screen(O)", cutCanvas);

        north.add(controLabel);
        north.add(new JLabel("      CLEAR(C)          "));
        north.add(cutCanvasLabel);

        frame.getContentPane().add(north, BorderLayout.NORTH);
        frame.getContentPane().add(glCanvas,BorderLayout.CENTER);
        frame.setSize(1000,800);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });


    }

    public static void setLabel(JLabel label, String str, boolean bool){
        if(bool){
            label.setText(str+": enabled");
        } else {
            label.setText(str+" : disabled");
        }
    }

    public static boolean isControl() {
        return control;
    }

    public static void setControl(boolean control) {
        MyRunnable.control = control;
    }

    public static boolean isCutCanvas() {
        return cutCanvas;
    }

    public static void setCutCanvas(boolean cutCanvas) {
        MyRunnable.cutCanvas = cutCanvas;
    }

    public static int getLimitXLeft() {
        return limitXLeft;
    }

    public static void setLimitXLeft(int limitXLeft) {
        MyRunnable.limitXLeft = limitXLeft;
    }

    public static int getLimitXRight() {
        return limitXRight;
    }

    public static void setLimitXRight(int limitXRight) {
        MyRunnable.limitXRight = limitXRight;
    }

    public static int getLimitYUp() {
        return limitYUp;
    }

    public static void setLimitYUp(int limitYUp) {
        MyRunnable.limitYUp = limitYUp;
    }

    public static int getLimitYDown() {
        return limitYDown;
    }

    public static void setLimitYDown(int limitYDown) {
        MyRunnable.limitYDown = limitYDown;
    }

    public static ArrayList<Line> getLines() {
        return lines;
    }
}
