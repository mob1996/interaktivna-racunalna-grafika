package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


/**
 * Created by Luka on 4.5.2017..
 */
public class MyRunnable implements Runnable{
    public static int width = 1000;
    public static int height = 800;
    public static double eps = 100;
    public static int m = 16;
    public static double umin = -1.5;
    public static double umax = 1.5;
    public static double vmin = -1;
    public static double vmax = 1;
    public static boolean mandelbrot = true;



    @Override
    public void run() {
        initGUI();
    }

    private void initGUI(){
        JFrame frame = new JFrame("Exercise 4.1.");
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLCanvas glCanvas =  new GLCanvas(glCapabilities);



        glCanvas.addGLEventListener(new MyGLEventListener());
        glCanvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if(e.getKeyChar()=='p'){
                    mandelbrot=!mandelbrot;
                }
                glCanvas.display();
            }
        });
        frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
        frame.setSize(width,height);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });
    }




}


