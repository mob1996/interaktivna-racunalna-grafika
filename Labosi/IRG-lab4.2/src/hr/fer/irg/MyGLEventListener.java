package hr.fer.irg;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import org.apache.commons.math3.complex.Complex;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Luka Mijić on 14.5.2017..
 */
public class MyGLEventListener implements GLEventListener {
    private GLU glu = new GLU();

    private double canvasWidth=1;
    private double canvasHeight=1;

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClearColor(1,1,1,0);
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClear (GL2.GL_COLOR_BUFFER_BIT);
        gl2.glLoadIdentity();
        if(MyRunnable.mandelbrot){
            renderMandelbrot(gl2);
        } else {
            renderJulij(gl2);
        }



    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        canvasWidth = width;
        canvasHeight = height;
        gl2.glDisable(GL2.GL_DEPTH_TEST);
        gl2.glViewport(0, 0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        gl2.glOrtho(0, width-1, height-1, 0, 0, 1);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);


    }

    private void renderMandelbrot(GL2 gl2){
        gl2.glBegin(GL2.GL_POINTS);
        for(int x=0;x<canvasWidth;x++){
            for(int y=0;y<canvasHeight;y++){

                double u0 = ((MyRunnable.umax-MyRunnable.umin)/(canvasWidth-1))*x+MyRunnable.umin;
                double v0 = ((MyRunnable.vmax-MyRunnable.vmin)/(canvasHeight-1))*y+MyRunnable.vmin;
                Complex c = new Complex(u0,v0);
                int n = divergence_test(c, MyRunnable.m);
                if(n==-1){
                    gl2.glColor3f(0,0,0);
                } else {
                    gl2.glColor3f((float)((double)n/MyRunnable.m),
                            (float)(1-((double)n/MyRunnable.m/2.0)),
                            (float)(0.8-((double)n/MyRunnable.m/3.0)));
                }
                gl2.glVertex2i(x,y);
            }
        }
        gl2.glEnd();
    }

    private int divergence_test(Complex c, int limit){
        Complex z = new Complex(0,0);
        for(int i=1;i<=limit;i++){

            z = new Complex(z.getReal()*z.getReal()-z.getImaginary()*z.getImaginary()+c.getReal(),
                    2*z.getReal()*z.getImaginary()+c.getImaginary());

            double r = Math.sqrt(Math.pow(z.getReal(),2)+Math.pow(z.getImaginary(),2));
            if(r>MyRunnable.eps){
                return i;
            }
        }
        return -1;
    }

    private void renderJulij(GL2 gl2){
        gl2.glBegin(GL2.GL_POINTS);
        for(int x=0;x<canvasWidth;x++){
            for(int y=0;y<canvasHeight;y++){

                double u0 = ((MyRunnable.umax-MyRunnable.umin)/(canvasWidth-1))*x+MyRunnable.umin;
                double v0 = ((MyRunnable.vmax-MyRunnable.vmin)/(canvasHeight-1))*y+MyRunnable.vmin;
                Complex z = new Complex(u0,v0);
                int n = divergence_testJulij(z, MyRunnable.m);
                if(n==-1){
                    gl2.glColor3f(0,0,0);
                } else {
                    gl2.glColor3f((float)((double)n/MyRunnable.m),
                            (float)(1-((double)n/MyRunnable.m/2.0)),
                            (float)(0.8-((double)n/MyRunnable.m/3.0)));
                }
                gl2.glVertex2i(x,y);
            }
        }
        gl2.glEnd();
    }

    private int divergence_testJulij(Complex z, int limit){
        Complex c = new Complex(0.32,0.043);
        for(int i=1;i<=limit;i++){

            z = new Complex(z.getReal()*z.getReal()-z.getImaginary()*z.getImaginary()+c.getReal(),
                    2*z.getReal()*z.getImaginary()+c.getImaginary());
            double r = Math.sqrt(Math.pow(z.getReal(),2)+Math.pow(z.getImaginary(),2));
            if(r>MyRunnable.eps){
                return i;
            }
        }
        return -1;
    }



}
