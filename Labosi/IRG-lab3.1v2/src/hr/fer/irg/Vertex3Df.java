package hr.fer.irg;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class Vertex3Df {
    private float x;
    private float y;
    private float z;

    public Vertex3Df(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    @Override
    public String toString(){
        return "v " + x + " " + y + " " + z;
    }
}
