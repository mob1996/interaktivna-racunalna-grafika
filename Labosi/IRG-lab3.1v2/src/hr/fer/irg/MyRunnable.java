package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Luka on 4.5.2017..
 */
public class MyRunnable implements Runnable{

    private static ArrayList<Vertex3Df> vertices = new ArrayList<>();
    public static ArrayList<TrianglePolygon> polygons = new ArrayList<>();

    public static Vertex3Df O = new Vertex3Df(1,1,3);
    public static Vertex3Df G = new Vertex3Df(0,0,0);

    RealMatrix modelViewTransform = new Array2DRowRealMatrix();
    RealMatrix projectionTransform = new Array2DRowRealMatrix();

    @Override
    public void run(){
        setMatrices();
        readFile();
        initGUI();
    }

    private void initGUI(){
        JFrame frame = new JFrame("Exercise 3.1.");
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLCanvas glCanvas =  new GLCanvas(glCapabilities);


        glCanvas.addGLEventListener(new MyGLEventListener());
        frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
        frame.setSize(800,800);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });
    }



    public void readFile(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("F:\\Faks\\Interaktivna računalna grafika\\Labosi\\IRG-lab3.1v2/temple.obj"));
            String line;
            while((line=reader.readLine())!=null){
                if(!(line.startsWith("v") || line.startsWith("f"))){
                    continue;
                }
                String[] tokens = line.split("\\s+");
                if(tokens[0].equals("v")){
                    processVertex(tokens);
                } else if(tokens[0].equals("f")){
                    processPolygon(tokens);
                }

            }
            reader.close();
        } catch (Exception e){
            System.out.println("Greska pri ocitavanju objekta");
            e.printStackTrace();
        }

    }

    private void processVertex(String[] tokens){
        float x = Float.valueOf(tokens[1]);
        float y = Float.valueOf(tokens[2]);
        float z = Float.valueOf(tokens[3]);
        double[][] point = {{x,y,z,1}};
        RealMatrix pointMatrix = new Array2DRowRealMatrix(point);
        pointMatrix = pointMatrix.multiply(modelViewTransform);
        pointMatrix = pointMatrix.multiply(projectionTransform);

        point = pointMatrix.getData();
        float h=(float)point[0][3];
        Vertex3Df vertex = new Vertex3Df((float)point[0][0]/h,(float)point[0][1]/h,(float)point[0][2]/h);
        System.out.println("["+x+" "+y+ " "+ z+"]------>"+"["+point[0][0]+" "+point[0][1]+" "+point[0][2]+" "+point[0][3]+"]");
        vertices.add(vertex);
    }

    private void processPolygon(String[] tokens){
        int indexOfFirst = Integer.valueOf(tokens[1].split("\\.")[0])-1;
        int indexOfSecond = Integer.valueOf(tokens[2].split("\\.")[0])-1;
        int indexOfThird =  Integer.valueOf(tokens[3].split("\\.")[0])-1;
        TrianglePolygon trianglePolygon = new TrianglePolygon(vertices.get(indexOfFirst), vertices.get(indexOfSecond), vertices.get(indexOfThird));
        polygons.add(trianglePolygon);
    }

    private void setMatrices(){
        double Ox = O.getX();
        double Oy = O.getY();
        double Oz = O.getZ();
        double Gx = G.getX();
        double Gy = G.getY();
        double Gz = G.getZ();
        double[][] translate = {{1, 0, 0, 0},
                                {0, 1, 0, 0},
                                {0, 0, 1, 0},
                                {-Ox,-Oy,-Oz, 1}};
        modelViewTransform = new Array2DRowRealMatrix(translate);

        Gx = Gx-Ox;
        Gy = Gy-Oy;
        Gz = Gz-Oz;

        double sinL = Gy/sqrt(pow(Gx,2)+pow(Gy,2));
        double cosL = Gx/sqrt(pow(Gx,2)+pow(Gy,2));
        Gx=sqrt(pow(Gx,2)+pow(Gy,2));
        Gy=0;
        Gz=Gz;

        double[][] rotateZ = {{cosL, -sinL, 0, 0},
                              {sinL, cosL, 0, 0},
                              {0, 0, 1, 0},
                              {0, 0, 0, 1}};
        modelViewTransform = modelViewTransform.multiply(new Array2DRowRealMatrix(rotateZ));

        double sinB = Gx/sqrt(pow(Gx,2)+pow(Gz,2));
        double cosB = Gz/sqrt(pow(Gx,2)+pow(Gz,2));
        double[][] rotateY = {{cosB, 0, sinB, 0},
                              {0, 1, 0, 0},
                              {-sinB, 0, cosB, 0},
                              {0, 0, 0, 1}};

        modelViewTransform = modelViewTransform.multiply(new Array2DRowRealMatrix(rotateY));

        double[][] T4 = {{0, -1, 0, 0},
                         {1, 0, 0, 0},
                         {0, 0, 1, 0},
                         {0, 0, 0, 1}};

        modelViewTransform = modelViewTransform.multiply(new Array2DRowRealMatrix(T4));

        double[][] T5 = {{-1, 0, 0, 0},
                       {0, 1, 0, 0},
                       {0, 0, 1, 0},
                       {0, 0, 0, 1}};

        modelViewTransform =  modelViewTransform.multiply(new Array2DRowRealMatrix(T5));

        double x_= pow(O.getX()-G.getX(),2);
        double y_= pow(O.getY()-G.getY(),2);
        double z_= pow(O.getZ()-G.getZ(),2);
        double H = sqrt(x_+y_+z_);

        double[][] projection = {{1,0,0,0},
                                 {0,1,0,0},
                                 {0,0,0,1/H},
                                 {0,0,0,0}};
        projectionTransform = new Array2DRowRealMatrix(projection);


        print4x4(modelViewTransform.getData());
        print4x4(projectionTransform.getData());
    }


    private void print4x4(double[][] matrix){
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                System.out.print(matrix[i][j]+"  ");
            }
            System.out.println();
        }
    }
}


