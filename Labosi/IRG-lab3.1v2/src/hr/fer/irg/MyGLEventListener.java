package hr.fer.irg;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;



/**
 * Created by Luka on 4.5.2017..
 */
public class MyGLEventListener implements GLEventListener {

    private GLU glu = new GLU();


    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClearColor(0,0,0,0);
        gl2.glClearDepth(1.0f);
        gl2.glEnable(GL.GL_DEPTH_TEST);
        gl2.glDepthFunc(GL.GL_LEQUAL);

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClear (GL2.GL_COLOR_BUFFER_BIT |  GL2.GL_DEPTH_BUFFER_BIT );
        gl2.glLoadIdentity();


        for(TrianglePolygon tp:MyRunnable.polygons){
            drawPolygon(tp, gl2);
        }

    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glViewport(0,0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        //glu.gluOrtho2D(-10, 10, 10, -10);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
    }



    private void printMatrix4x4(double[] matrix){
        for(int i=0;i<16;i++){
            if(i%4==0){
                System.out.println();
            }
            System.out.print(matrix[i]+"  ");
        }
        System.out.println();
    }

    private void drawPolygon(TrianglePolygon tp, GL2 gl2){
        gl2.glBegin(GL2.GL_LINE_LOOP);
        gl2.glColor3f(1, 1, 0);
        gl2.glVertex2f(tp.getFirst().getX(),
                tp.getFirst().getY());
        gl2.glVertex2f(tp.getSecond().getX(),
                tp.getSecond().getY());
        gl2.glVertex2f(tp.getThird().getX(),
                tp.getThird().getY());
        gl2.glEnd();

    }
}

