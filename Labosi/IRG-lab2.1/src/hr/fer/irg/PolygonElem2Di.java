package hr.fer.irg;

/**
 * Created by Luka Mijić on 29.3.2017..
 */
public class PolygonElem2Di {
    Point2Di startVertex;
    Edge2Di edge;
    boolean leftVertex;

    public PolygonElem2Di(Point2Di startVertex, Edge2Di edge, boolean leftVertex) {
        this.startVertex = startVertex;
        this.edge = edge;
        this.leftVertex = leftVertex;

    }


    public Point2Di getStartVertex() {
        return startVertex;
    }

    public void setStartVertex(Point2Di startVertex) {
        this.startVertex = startVertex;
    }

    public Edge2Di getEdge() {
        return edge;
    }

    public void setEdge(Edge2Di edge) {
        this.edge = edge;
    }

    public boolean isLeftVertex() {
        return leftVertex;
    }

    public void setLeftVertex(boolean leftVertex) {
        this.leftVertex = leftVertex;
    }
}
