package hr.fer.irg;

/**
 * Created by Luka Mijić on 29.3.2017..
 */
public class Point2Di {

    private int x;
    private int y;

    public Point2Di(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
