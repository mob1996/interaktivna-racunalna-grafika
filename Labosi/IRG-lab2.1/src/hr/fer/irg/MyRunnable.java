package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.sun.javafx.geom.Edge;
import com.sun.org.apache.xpath.internal.SourceTree;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by Luka Mijić on 29.3.2017..
 */
public class MyRunnable implements Runnable {
    private static ArrayList<Point2Di> points =  new ArrayList<>();
    private static ArrayList<Edge2Di> edges = new ArrayList<>();
    public static ArrayList<PolygonElem2Di> polyElems= new ArrayList<>();

    public static int minX = Integer.MAX_VALUE;
    public static int maxX = 0;
    public static int minY = Integer.MAX_VALUE;
    public static int maxY = 0;

    private boolean vertexInput= true;
    private static boolean pointInput=false;
    public static boolean pointSet=false;

    public static Point2Di point;

    @Override
    public void run() {
        JFrame frame = new JFrame("PolygonExercise");
        JPanel north = new JPanel(new FlowLayout());
        JPanel south = new JPanel(new FlowLayout());
        JButton reset = new JButton("Reset");
        JLabel verticesLabel = new JLabel("Number of vertices: "+points.size());
        JLabel task = new JLabel("Enter vertices of polygon!        ");

        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities =  new GLCapabilities(glProfile);
        GLCanvas glCanvas = new GLCanvas(glCapabilities);

        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                points.clear();
                edges.clear();
                polyElems.clear();
                verticesLabel.setText("Number of vertices: "+points.size());
                task.setText("Enter vertices of polygon!        ");
                vertexInput=true;
                pointInput=false;
                pointSet=false;
                minX = Integer.MAX_VALUE;
                maxX = 0;
                minY = Integer.MAX_VALUE;
                maxY = 0;
                glCanvas.display();
            }
        });

        glCanvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = glCanvas.getSurfaceHeight()-e.getY();
                if(vertexInput==true){
                    setMinMax(x,y);
                    System.out.println("("+x+","+y+")");
                    Point2Di p = new Point2Di(x, y);
                    points.add(p);
                    verticesLabel.setText("Number of vertices: "+points.size());
                }
                if(pointInput==true){
                    point = new Point2Di(x,y);
                    pointSet=true;
                    glCanvas.display();
                }
            }
        });

        glCanvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_D){
                    vertexInput=false;
                    pointInput=true;
                    System.out.println("MinX = "+minX);
                    System.out.println("MaxX = "+maxX);
                    System.out.println("MinY = "+minY);
                    System.out.println("MaxY = "+maxY);
                    calculatePolyElems();
                    task.setText("Input checking point!");
                    glCanvas.display();
                }
            }
        });

        glCanvas.addGLEventListener(new MyGLEventListener(glCanvas));
        south.add(task);
        north.add(reset);
        north.add(new JLabel("      Press 'D' to draw poligon    "));
        north.add(verticesLabel);

        frame.getContentPane().add(north, BorderLayout.NORTH);
        frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
        frame.getContentPane().add(south, BorderLayout.SOUTH);
        frame.setSize(1000,800);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });



    }

    private void setMinMax(int x, int y) {
        if (x > maxX) {
            maxX = x;
        }
        if (x < minX) {
            minX = x;
        }
        if (y > maxY) {
            maxY = y;
        }
        if (y < minY) {
            minY = y;
        }
    }

    private void calculatePolyElems(){
        int n = points.size();
        for(int i=0;i<n;i++){
            Point2Di start =  points.get(i);
            Point2Di end = points.get((i+1)%n);
            int a = start.getY()-end.getY();
            int b = -(start.getX()-end.getX());
            int c = start.getX()*end.getY()-start.getY()*end.getX();
            Edge2Di edge =  new Edge2Di(a,b,c);
            edges.add(edge);
            boolean left =start.getY()<end.getY();
            System.out.println(left);
            polyElems.add(new PolygonElem2Di(start, edge, left));
        }
    }


}
