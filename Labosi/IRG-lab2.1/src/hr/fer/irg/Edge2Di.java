package hr.fer.irg;

/**
 * Created by Luka Mijić on 29.3.2017..
 */
public class Edge2Di {
    private int a;
    private int b;
    private int c;

    public Edge2Di(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }
}
