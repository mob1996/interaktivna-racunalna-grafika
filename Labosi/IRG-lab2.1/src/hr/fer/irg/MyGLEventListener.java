package hr.fer.irg;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;

import java.util.ArrayList;

/**
 * Created by Luka Mijić on 29.3.2017..
 */
public class MyGLEventListener implements GLEventListener{

    GLCanvas glCanvas;

    public MyGLEventListener(GLCanvas glCanvas){
        this.glCanvas = glCanvas;
    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();

        gl2.glClearColor(1, 1, 1, 1);
        gl2.glLoadIdentity();
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT);

        drawPolygon(gl2);
        if(MyRunnable.pointSet){
            gl2.glBegin(GL2.GL_POINTS);
            gl2.glPointSize(3);
            gl2.glVertex2i(MyRunnable.point.getX(), MyRunnable.point.getY());
            gl2.glPointSize(1);
            gl2.glEnd();
            checkIfInsidePolygon(MyRunnable.point);
        }
        if(MyRunnable.polyElems.size()>0){
            colorPolygon(gl2);
        }
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();

        gl2.glDisable(GL2.GL_DEPTH_TEST);
        gl2.glViewport(0, 0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        gl2.glOrtho(0, width-1, 0, height-1, 0, 1);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
    }

    private void drawPolygon(GL2 gl2){
        gl2.glBegin(GL2.GL_LINE_LOOP);
        gl2.glColor3f(0,0,0);

        for(PolygonElem2Di elem:MyRunnable.polyElems){
            Point2Di p = elem.getStartVertex();
            gl2.glVertex2i(p.getX(), p.getY());
        }
        gl2.glEnd();

    }

    private void checkIfInsidePolygon(Point2Di point){
        for(PolygonElem2Di elem:MyRunnable.polyElems){
            Edge2Di edge = elem.getEdge();
            int check = point.getX()*edge.getA()+point.getY()*edge.getB()+edge.getC();
            if(check>0){
                System.out.println("Točka ("+point.getX()+","+point.getY()+") je izvan poligona!");
                return;
            }
        }
        System.out.println("Točka ("+point.getX()+","+point.getY()+") je unutar poligona!");
    }

    private void colorPolygon(GL2 gl2){
        ArrayList<PolygonElem2Di> polys = MyRunnable.polyElems;
        int i0;
        int xmin = MyRunnable.minX;
        int xmax = MyRunnable.maxX;
        int ymin = MyRunnable.minY;
        int ymax = MyRunnable.maxY;
        int nVertices = polys.size();

        double L, D, x;

        gl2.glBegin(GL2.GL_LINES);
        gl2.glColor3f(0,0,0);

        for(int y=ymin; y<=ymax;y++){
            L=xmin;
            D=xmax;
            i0=nVertices-1;
            for(int i=0;i<nVertices;i0=i++){
                if(polys.get(i0).edge.getA()==0){
                    if(polys.get(i0).getStartVertex().getY()==y){
                        if(polys.get(i0).getStartVertex().getX()<polys.get(i).getStartVertex().getX()){
                            L=polys.get(i0).getStartVertex().getX();
                            D=polys.get(i).getStartVertex().getX();
                        } else {
                            L=polys.get(i).getStartVertex().getX();
                            D=polys.get(i0).getStartVertex().getX();
                        }
                        break;
                    }
                } else {
                    x=(-polys.get(i0).edge.getB()*y-polys.get(i0).edge.getC())/(double)polys.get(i0).getEdge().getA();
                    if(polys.get(i0).isLeftVertex()){
                        if(L<x)
                            L=x;
                    } else {
                        if(D>x)
                            D=x;
                    }

                }
            }
            gl2.glVertex2i((int)L,y);
            gl2.glVertex2i((int)D,y);
        }
        gl2.glEnd();
    }
}
