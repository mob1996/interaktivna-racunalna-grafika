package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by Luka Mijić on 14.5.2017..
 */
public class MyRunnable implements Runnable {

    private static ArrayList<Vertex3Df> vertices = new ArrayList<>();
    public static ArrayList<TrianglePolygon> polygons = new ArrayList<>();
    public static ArrayList<Vertex3Df> bezierVertices = new ArrayList<>();
    private double xmin = Double.MAX_VALUE;
    private double xmax = Double.MIN_VALUE;
    private double ymin = Double.MAX_VALUE;
    private double ymax = Double.MIN_VALUE;
    private double zmin = Double.MAX_VALUE;
    private double zmax = Double.MIN_VALUE;
    private boolean fitScreen=false;

    @Override
    public void run() {
        readObjectFile();
        calculatePolyKoefs();
        readBezier();
        initGUI();
    }

    private void initGUI(){
        JFrame frame = new JFrame("Exercise 3.1.");
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLCanvas glCanvas =  new GLCanvas(glCapabilities);

        glCanvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_E){
                    if(MyGLEventListener.T+0.05<1.04){
                        MyGLEventListener.T+=0.05;
                    }
                } else if(e.getKeyCode()==KeyEvent.VK_Q){
                    if(MyGLEventListener.T-0.05>-0.04){
                        MyGLEventListener.T-=0.05;
                    }
                }
                System.out.println(MyGLEventListener.T);
                glCanvas.display();
            }
        });
        glCanvas.addGLEventListener(new MyGLEventListener());
        frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
        frame.setSize(800,800);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });
    }


    public void readObjectFile(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("F:\\Faks\\Interaktivna računalna grafika\\Labosi\\IRG-lab3.1v2\\temple.obj"));
            String line;
            while((line=reader.readLine())!=null){
                if(!(line.startsWith("v") || line.startsWith("f"))){
                    continue;
                }
                String[] tokens = line.split("\\s+");
                if(tokens[0].equals("v")){
                    processVertex(tokens);
                } else if(tokens[0].equals("f")){
                    if(fitScreen==false){
                        pointsFitScreen();
                        fitScreen=true;
                    }
                    processPolygon(tokens);
                }

            }
            reader.close();
        } catch (Exception e){
            System.out.println("Greska pri ocitavanju objekta");
            e.printStackTrace();
        }

    }

    private void processVertex(String[] tokens){
        float x = Float.valueOf(tokens[1]);
        float y = Float.valueOf(tokens[2]);
        float z = Float.valueOf(tokens[3]);
        if(x<xmin) xmin=x;
        if(x>xmax) xmax=x;
        if(y<ymin) ymin=y;
        if(y>ymax) ymax=y;
        if(z<zmin) zmin=z;
        if(z>zmax) zmax=z;

        Vertex3Df vertex = new Vertex3Df(x,y,z);
        vertices.add(vertex);
    }

    private void processPolygon(String[] tokens){
        int indexOfFirst = Integer.valueOf(tokens[1].split("\\.")[0])-1;
        int indexOfSecond = Integer.valueOf(tokens[2].split("\\.")[0])-1;
        int indexOfThird =  Integer.valueOf(tokens[3].split("\\.")[0])-1;
        TrianglePolygon trianglePolygon = new TrianglePolygon(vertices.get(indexOfFirst), vertices.get(indexOfSecond), vertices.get(indexOfThird));
        polygons.add(trianglePolygon);
    }

    private void pointsFitScreen(){
        double velicina_x = xmax-xmin;
        double velicina_y = ymax-ymin;
        double velicina_z = ymax-ymin;

        double maxVelicina = velicina_x;
        if(velicina_y>maxVelicina) maxVelicina=velicina_y;
        if(velicina_z>maxVelicina) maxVelicina=velicina_z;

        double scaleValue = 2/maxVelicina;

        double srediste_x = (xmax+xmin)/2;
        double srediste_y = (ymax+ymin)/2;
        double srediste_z = (zmax+zmin)/2;

        double[][] translate = {{1, 0, 0, 0},
                                {0, 1, 0, 0},
                                {0, 0, 1, 0},
                                {-srediste_x,-srediste_y,-srediste_z, 1}};
        double[][] scale = {{scaleValue, 0, 0, 0},
                            {0, scaleValue, 0, 0},
                            {0, 0, scaleValue, 0},
                            {0, 0, 0, 1}};
        RealMatrix transformPoints = new Array2DRowRealMatrix(translate);
        transformPoints=transformPoints.multiply(new Array2DRowRealMatrix(scale));
        for(Vertex3Df v:vertices){
            double x = v.getX();
            double y = v.getY();
            double z = v.getZ();
            double[][] point = {{x,y,z,1}};
            RealMatrix pointMatrix = new Array2DRowRealMatrix(point);
            pointMatrix = pointMatrix.multiply(transformPoints);
            point = pointMatrix.getData();
            float h = (float)point[0][3];
            v.setX((float)point[0][0]/h);
            v.setY((float)point[0][1]/h);
            v.setZ((float)point[0][2]/h);
            //System.out.println(v.getX()+" "+v.getY()+" "+v.getZ());

        }

    }

    private void calculatePolyKoefs(){
        for(TrianglePolygon tp:polygons){
            tp.calculateKoefs();
        }
    }

    private void readBezier(){
        try{
            BufferedReader reader = new BufferedReader(new FileReader("F:\\Faks\\Interaktivna računalna grafika\\Labosi\\IRG-lab3.2/bezier2.txt"));
            String line;
            while((line=reader.readLine())!=null){
                String[] tokens = line.split("\\s+");
                float x = Float.valueOf(tokens[0]);
                float y = Float.valueOf(tokens[1]);
                float z = Float.valueOf(tokens[2]);
                bezierVertices.add(new Vertex3Df(x,y,z));
            }
            reader.close();
        } catch (Exception e){
            System.out.println("Greska pri otvaranju bezier.txt");
            e.printStackTrace();
        }
    }


}
