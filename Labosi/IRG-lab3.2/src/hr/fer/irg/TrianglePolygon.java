package hr.fer.irg;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class TrianglePolygon {
    private Vertex3Df first;
    private Vertex3Df second;
    private Vertex3Df third;

    private float koefA;
    private float koefB;
    private float koefC;
    private float koefD;

    public TrianglePolygon(Vertex3Df first, Vertex3Df second, Vertex3Df third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public Vertex3Df getFirst() {
        return first;
    }

    public Vertex3Df getSecond() {
        return second;
    }

    public Vertex3Df getThird() {
        return third;
    }

    public float getKoefA() {
        return koefA;
    }

    public float getKoefB() {
        return koefB;
    }

    public float getKoefC() {
        return koefC;
    }

    public float getKoefD() {
        return koefD;
    }

    public void calculateKoefs(){
        koefA=(second.getY()-first.getY())*(third.getZ()-first.getZ())-(second.getZ()-first.getZ())*(third.getY()-first.getY());
        koefB=-(second.getX()-first.getX())*(third.getZ()-first.getZ())+(second.getZ()-first.getZ())*(third.getX()-first.getX());
        koefC=(second.getX()-first.getX())*(third.getY()-first.getY())-(second.getY()-first.getY())*(third.getX()-first.getX());
        koefD=-first.getX()*koefA-first.getY()*koefB-first.getZ()*koefC;
    }

    @Override
    public String toString(){
        return first +" :1\n"+second+" :2\n"+third+" :3\n";
    }
}
