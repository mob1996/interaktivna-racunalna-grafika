package hr.fer.irg;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import org.apache.commons.math3.util.CombinatoricsUtils;
import static java.lang.Math.pow;

/**
 * Created by Luka Mijić on 14.5.2017..
 */
public class MyGLEventListener implements GLEventListener {
    private Vertex3Df O = new Vertex3Df(MyRunnable.bezierVertices.get(0).getX(),MyRunnable.bezierVertices.get(0).getY(),MyRunnable.bezierVertices.get(0).getZ());
    public static double T=0;
    private double step=0.01;
    private GLU glu = new GLU();
    private double width =1;
    private double height=1;

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClearColor(0,0,0,0);
        gl2.glClearDepth(1.0f);
        gl2.glEnable(GL.GL_DEPTH_TEST);
        gl2.glDepthFunc(GL.GL_LEQUAL);
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        calculateO();
        gl2.glClear (GL2.GL_COLOR_BUFFER_BIT |  GL2.GL_DEPTH_BUFFER_BIT );
        gl2.glLoadIdentity();
        glu.gluLookAt(O.getX(), O.getY(), O.getZ(),0,0,0,0,1,0);
        gl2.glScalef(0.2f,0.2f,0.2f);
        renderBezier(gl2);
        renderObj(gl2);
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        calculateO();
        gl2.glViewport(0,0,width,height);
        this.width=width;
        this.height=height;
        gl2.glEnable(GL2.GL_DEPTH_TEST);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        glu.gluPerspective(45.0, (float)width/height, 0.1f, 100f);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);

    }





    private void calculateO(){
        double x=0;
        double y=0;
        double z=0;
        int n=MyRunnable.bezierVertices.size()-1;
        int i=0;
        for(Vertex3Df v:MyRunnable.bezierVertices){
            double b = CombinatoricsUtils.binomialCoefficientDouble(n,i)*pow(T,i)*pow((1-T),n-i);
            x+=v.getX()*b;
            y+=v.getY()*b;
            z+=v.getZ()*b;
            i++;
            System.out.println(v.getX()+"  "+v.getY()+"   "+v.getZ());
        }
        System.out.println("O:"+x+" "+y+" "+z+"   T: "+T);
        O.setX((float)x);
        O.setY((float)y);
        O.setZ((float)z);
    }

    private void renderBezier(GL2 gl2){
        gl2.glColor3f(1,0,0);
        gl2.glPointSize(3.0f);
        gl2.glBegin(GL2.GL_POINTS);
        for(double t=0;t<=1;t+=0.01){
            double x=0;
            double y=0;
            double z=0;
            int n=MyRunnable.bezierVertices.size()-1;
            int i=0;
            for(Vertex3Df v: MyRunnable.bezierVertices){
                double b = CombinatoricsUtils.binomialCoefficientDouble(n,i)*pow(t,i)*pow((1-t),n-i);
                x+=v.getX()*b;
                y+=v.getY()*b;
                z+=v.getZ()*b;
                i++;
            }
            //System.out.println(x+" "+y+" "+z);
            gl2.glVertex3f((float)x,(float)y,(float)z);
        }
        gl2.glEnd();
    }

    private void renderObj(GL2 gl2){
        gl2.glColor3f(1,1,0);
        gl2.glPointSize(1.0f);
        gl2.glBegin(GL2.GL_LINE_LOOP);
        for(TrianglePolygon tp:MyRunnable.polygons){
            gl2.glVertex3f(tp.getFirst().getX(),
                    tp.getFirst().getY(),
                    tp.getFirst().getZ());
            gl2.glVertex3f(tp.getSecond().getX(),
                    tp.getSecond().getY(),
                    tp.getSecond().getZ());
            gl2.glVertex3f(tp.getThird().getX(),
                    tp.getThird().getY(),
                    tp.getThird().getZ());
        }
        gl2.glEnd();
    }
}
