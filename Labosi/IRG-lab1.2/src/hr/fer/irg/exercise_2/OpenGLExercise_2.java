package hr.fer.irg.exercise_2;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import hr.fer.irg.Color.Color;
import hr.fer.irg.basicGeometry.Point;
import hr.fer.irg.basicGeometry.Triangle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by Luka on 9.3.2017..
 */
public class OpenGLExercise_2 {

    private static ArrayList<Color> colors;
    static {
        colors = new ArrayList<>();
        colors.add(new Color(1, 0, 0));
        colors.add(new Color(0, 1, 0));
        colors.add(new Color(0, 0, 1));
        colors.add(new Color(0, 1, 1));
        colors.add(new Color(1, 1, 0));
        colors.add(new Color(1, 0, 1));
    }

    private static int colorPointer = 0;
    private static int squareDimension = 50;

    private static ArrayList<Triangle> triangles = new ArrayList<>();
    private static ArrayList<Point> points = new ArrayList<>();


    public static void main(String[] args){


        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                GLProfile glProfile = GLProfile.getDefault();
                GLCapabilities glCapabilities = new GLCapabilities(glProfile);
                GLCanvas glCanvas = new GLCanvas(glCapabilities);

                glCanvas.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if(e.getX()<=squareDimension && e.getY()<=squareDimension){
                            return;
                        }
                        Point p = new Point(e.getX(),e.getY());
                        points.add(p);
                        if(points.size()%3==0){
                            triangles.add(new Triangle(points.get(0), points.get(1), points.get(2), colors.get(colorPointer)));
                            points.clear();
                            glCanvas.display();
                        }
                    }

                });

                glCanvas.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        if(e.getKeyCode()==KeyEvent.VK_N){
                            colorPointer = (colorPointer+1)%colors.size();
                        } else if(e.getKeyCode()==KeyEvent.VK_P){
                            colorPointer--;
                            if(colorPointer<0){
                                colorPointer=colors.size()-1;
                            }
                        } else if(e.getKeyCode()==KeyEvent.VK_C){
                            triangles.clear();
                        }
                        glCanvas.display();
                    }
                });

                glCanvas.addGLEventListener(new GLEventListener() {
                    @Override
                    public void init(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void dispose(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void display(GLAutoDrawable glAutoDrawable) {
                        GL2 gl2 = glAutoDrawable.getGL().getGL2();
                        Color quadColor = colors.get(colorPointer);

                        gl2.glClearColor(1, 1, 1, 1);
                        gl2.glLoadIdentity();
                        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT);

                        gl2.glBegin(GL2.GL_QUADS);
                        gl2.glColor3f(quadColor.getRed(), quadColor.getGreen(), quadColor.getBlue());
                        gl2.glVertex2i(0, 0);
                        gl2.glVertex2i(0, squareDimension);
                        gl2.glVertex2i(squareDimension, squareDimension);
                        gl2.glVertex2i(squareDimension, 0);
                        gl2.glEnd();

                        gl2.glLoadIdentity();
                        gl2.glBegin(GL2.GL_TRIANGLES);

                        for(Triangle t:triangles){
                            Color tColor = t.getColor();
                            gl2.glColor3f(tColor.getRed(), tColor.getGreen(), tColor.getBlue());
                            gl2.glVertex2f(t.getFirst().getX(), t.getFirst().getY());
                            gl2.glVertex2f(t.getSecond().getX(), t.getSecond().getY());
                            gl2.glVertex2i(t.getThird().getX(), t.getThird().getY());
                        }
                        gl2.glEnd();
                    }

                    @Override
                    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                        GL2 gl2 = glAutoDrawable.getGL().getGL2();

                        gl2.glDisable(GL2.GL_DEPTH_TEST);
                        gl2.glViewport(0, 0, width, height);
                        gl2.glMatrixMode(GL2.GL_PROJECTION);
                        gl2.glLoadIdentity();
                        gl2.glOrtho(0, width-1, height-1, 0, 0, 1);
                        gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    }
                });



                JFrame frame = new JFrame("Vjezba_2");

                frame.setSize(800, 600);
                frame.setVisible(true);
                frame.addWindowListener(new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent e) {
                        frame.dispose();
                        System.exit(0);
                    }

                });
                frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
                glCanvas.requestFocusInWindow();
            }
        });

    }
}
