package hr.fer.irg.basicGeometry;

/**
 * Created by Luka on 9.3.2017..
 */
public class Point {
    private int x;
    private int y;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }
}
