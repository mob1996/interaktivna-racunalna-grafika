package hr.fer.irg.basicGeometry;


import hr.fer.irg.Color.Color;

/**
 * Created by Luka on 9.3.2017..
 */
public class Triangle {

    private Point first;
    private Point second;
    private Point third;
    private Color color;


    public Triangle(Point first, Point second, Point third, Color color) {
        this.first = first;
        this.second = second;
        this.third = third;
        this.color = color;
    }


    public Point getFirst() {
        return first;
    }


    public Point getSecond() {
        return second;
    }


    public Point getThird() {
        return third;
    }


    public Color getColor() {
        return color;
    }
}
