package hr.fer.irg.Color;

/**
 * Created by Luka on 9.3.2017..
 */
public class Color {
    private float red;
    private float green;
    private float blue;

    public Color(float red, float green, float blue){
        if((red<0 || red>1) || (green<0 || green>1) || (blue<0 || blue>1)){
            throw new InvalidColorException("Color value must be between 0.0 and 1.0.");
        }
        this.red=red;
        this.green=green;
        this.blue=blue;
    }

    public float getRed() {
        return red;
    }

    public float getGreen() {
        return green;
    }

    public float getBlue() {
        return blue;
    }
}
