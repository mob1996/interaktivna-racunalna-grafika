package hr.fer.irg.Color;

/**
 * Created by Luka on 9.3.2017..
 */
public class InvalidColorException extends RuntimeException {
    public InvalidColorException(String string) {
        super(string);
    }
}
