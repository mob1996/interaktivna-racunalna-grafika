package hr.fer.irg;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class Vertex3Di {
    private int x;
    private int y;
    private int z;

    public Vertex3Di(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public String toString(){
        return "v " + x + " " + y + " " + z;
    }
}
