package hr.fer.irg;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;

import java.util.Random;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class MyGLEventListener implements GLEventListener {
    private GLU glu = new GLU();
    private float rotate = 0;
    private static float[][] colors= new float[12][3];
    static{
        Random rand = new Random();
        for(int i=0;i<12;i++){
            for(int j=0;j<3;j++){
                colors[i][j]=rand.nextFloat();
            }
        }
    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glShadeModel(GL2.GL_SMOOTH);
        gl2.glClearColor(1,1,1,0);
        gl2.glClearDepth(1.0f);
        gl2.glEnable(GL.GL_DEPTH_TEST);
        gl2.glDepthFunc(GL.GL_LEQUAL);
        gl2.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl2.glLoadIdentity();
        if(MyRunnable.polygons.size()==12){
            gl2.glTranslatef(-0.5f,-0.5f, -7);
        } else {
            gl2.glTranslatef(-1,-0f, -7);
        }

        gl2.glRotatef(rotate,1,1,0);
        gl2.glColor3f(1, 1, 1);
        int i=0;
        for(TrianglePolygon tp:MyRunnable.polygons){
            drawPolygon(tp,gl2, i);
            i++;
        }
        rotate+=0.5f;

    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        if(height==0){
            height=1;
        }
        gl2.glViewport(0,0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();

        glu.gluPerspective(45.0f, width/height, 0.1f, 100.f);

        gl2.glMatrixMode(GL2.GL_MODELVIEW);
        gl2.glLoadIdentity();
    }

    private void drawPolygon(TrianglePolygon tp, GL2 gl2,int i){
        Random rand = new Random();
        gl2.glBegin(GL2.GL_TRIANGLES);
        gl2.glColor3f(colors[i][0],colors[i][1], colors[i][2]);
        gl2.glVertex3i(tp.getFirst().getX(),
                tp.getFirst().getY(),
                tp.getFirst().getZ());
        gl2.glVertex3i(tp.getSecond().getX(),
                tp.getSecond().getY(),
                tp.getSecond().getZ());
        gl2.glVertex3i(tp.getThird().getX(),
                tp.getThird().getY(),
                tp.getThird().getZ());
        gl2.glEnd();

    }
}
