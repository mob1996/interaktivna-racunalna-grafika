package hr.fer.irg;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class MyGLEventListener2 implements GLEventListener {
    GLU glu = new GLU();
    float rotate = 0.10f;
    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glShadeModel(GL2.GL_SMOOTH);
        gl2.glClearColor(1,1,1,0);
        gl2.glClearDepth(1.0f);
        gl2.glEnable(GL.GL_DEPTH_TEST);
        gl2.glDepthFunc(GL.GL_LEQUAL);
        gl2.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl2.glLoadIdentity();
        gl2.glRotatef(rotate,1,1,1);
        gl2.glTranslatef(0,0,-0.1f);
        gl2.glColor3f(0,0,0);
        gl2.glBegin(GL2.GL_TRIANGLES);
        gl2.glVertex2f(0,0.5f);
        gl2.glVertex2f(-0.5f,-0.5f);
        gl2.glVertex2f(0.5f, -0.5f);
        gl2.glEnd();
        rotate+=0.1;
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl2 = glAutoDrawable.getGL().getGL2();
        if(height==0){
            height=1;
        }
        gl2.glViewport(0,0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();

        glu.gluPerspective(45.0f, width/height, 0.1f, 100.f);

        gl2.glMatrixMode(GL2.GL_MODELVIEW);
        gl2.glLoadIdentity();
    }
}
