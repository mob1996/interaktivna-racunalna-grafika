package hr.fer.irg;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class TrianglePolygon {
    private Vertex3Di first;
    private Vertex3Di second;
    private Vertex3Di third;

    private int koefA;
    private int koefB;
    private int koefC;
    private int koefD;

    public TrianglePolygon(Vertex3Di first, Vertex3Di second, Vertex3Di third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public Vertex3Di getFirst() {
        return first;
    }

    public Vertex3Di getSecond() {
        return second;
    }

    public Vertex3Di getThird() {
        return third;
    }

    public int getKoefA() {
        return koefA;
    }

    public int getKoefB() {
        return koefB;
    }

    public int getKoefC() {
        return koefC;
    }

    public int getKoefD() {
        return koefD;
    }

    public void calculateKoefs(){
        koefA=(second.getY()-first.getY())*(third.getZ()-first.getZ())-(second.getZ()-first.getZ())*(third.getY()-first.getY());
        koefB=-(second.getX()-first.getX())*(third.getZ()-first.getZ())+(second.getZ()-first.getZ())*(third.getX()-first.getX());
        koefC=(second.getX()-first.getX())*(third.getY()-first.getY())-(second.getY()-first.getY())*(third.getX()-first.getX());
        koefD=-first.getX()*koefA-first.getY()*koefB-first.getZ()*koefC;
    }

    @Override
    public String toString(){
        return first +" :1\n"+second+" :2\n"+third+" :3\n";
    }
}
