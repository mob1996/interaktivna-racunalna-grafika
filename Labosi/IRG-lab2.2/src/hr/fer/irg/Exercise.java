package hr.fer.irg;

import javax.swing.*;
import java.util.Scanner;


/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class Exercise {

    public static void main(String[] args){
        SwingUtilities.invokeLater(new MyRunnable());

        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("Enter x, y, z for checking: ");
            double x = sc.nextDouble();
            double y = sc.nextDouble();
            double z = sc.nextDouble();
            checkIfInside(x,y,z);
        }
    }

    private static void checkIfInside(double x, double y, double z){
        for(TrianglePolygon p:MyRunnable.polygons){
            double check = x*p.getKoefA()+y*p.getKoefB()+z*p.getKoefC()+p.getKoefD();
            if(check>0){
                System.out.println("Točka je izvan tijela.");
                return;
            }
        }
        System.out.println("Točka je unutar tijela.");
    }
}
