package hr.fer.irg;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Luka Mijić on 2.4.2017..
 */
public class MyRunnable implements Runnable {

    private static ArrayList<Vertex3Di> vertices = new ArrayList<>();
    public static ArrayList<TrianglePolygon> polygons = new ArrayList<>();


    @Override
    public void run() {
        readFile();
        for(TrianglePolygon tp:polygons){
            tp.calculateKoefs();
        }

        JFrame frame = new JFrame("Exercise");
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities =  new GLCapabilities(glProfile);
        GLCanvas glCanvas = new GLCanvas(glCapabilities);
        FPSAnimator animator =  new FPSAnimator(glCanvas,100,true);

        glCanvas.addGLEventListener(new MyGLEventListener());
        frame.getContentPane().add(glCanvas, BorderLayout.CENTER);
        frame.setSize(800,800);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }

        });
        animator.start();



    }



    public void readFile(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("D:\\Faks\\Interaktivna računalna grafika\\Labosi\\IRG-lab2.2\\kocka.obj"));
            String line;
            while((line=reader.readLine())!=null && !line.isEmpty()){
                if(!(line.startsWith("v") || line.startsWith("f"))){
                    continue;
                }
                String[] tokens = line.split("\\s+");
                if(tokens[0].equals("v")){
                    processVertex(tokens);
                } else if(tokens[0].equals("f")){
                    processPolygon(tokens);
                }

            }
            reader.close();
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    private void processVertex(String[] tokens){
        int x = Integer.valueOf(tokens[1].split("\\.")[0]);
        int y = Integer.valueOf(tokens[2].split("\\.")[0]);
        int z = Integer.valueOf(tokens[3].split("\\.")[0]);
        Vertex3Di vertex = new Vertex3Di(x,y,z);
        vertices.add(vertex);
    }

    private void processPolygon(String[] tokens){
        int indexOfFirst = Integer.valueOf(tokens[1].split("\\.")[0])-1;
        int indexOfSecond = Integer.valueOf(tokens[2].split("\\.")[0])-1;
        int indexOfThird =  Integer.valueOf(tokens[3].split("\\.")[0])-1;
        TrianglePolygon trianglePolygon = new TrianglePolygon(vertices.get(indexOfFirst), vertices.get(indexOfSecond), vertices.get(indexOfThird));
        polygons.add(trianglePolygon);
    }



}
